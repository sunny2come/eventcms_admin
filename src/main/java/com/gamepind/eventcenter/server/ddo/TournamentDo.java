package com.gamepind.eventcenter.server.ddo;

import java.io.Serializable;

import com.gamepind.eventcenter.server.entity.Tournament;

public class TournamentDo extends Tournament implements Serializable {

	private String iconUrl;
	private String gameName;
	private Integer currentPlayer;
	private boolean partake;//该用户是否参与过当前tournament

	public String getGameUrl() {
		return gameUrl;
	}

	public void setGameUrl(String gameUrl) {
		this.gameUrl = gameUrl;
	}

	private String gameUrl;

	public boolean isPartake() {
		return partake;
	}

	public void setPartake(boolean partake) {
		this.partake = partake;
	}

	public Integer getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Integer currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

}
