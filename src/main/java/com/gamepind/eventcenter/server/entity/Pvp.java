package com.gamepind.eventcenter.server.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Pvp implements Serializable{
    private Integer id;

    private String name;

    private Integer gameId;

    private Date startTime;

    private Date endTime;

    private Byte entryType;

    private BigDecimal entryFee;

    private String entryProductId;

    private Byte rewardType;

    private BigDecimal reward;

    private Byte platformBenefitType;

    private BigDecimal platformBenefit;

    private Integer coinPoolSource;

    private Byte status;

    private Date createTime;

    private Date onlineTime;

    private Date offlineTime;

    private Integer forder;

    private BigDecimal rake;

    private Byte roundCount;

    private Integer dislodge;

    private String channel;

    private Integer roundTime;

    private Integer benefitTimes;

    private Byte pvpType;

    private Boolean botSupport;

    private Integer botNumbers;

    private String pvpUrl;
    private String gameName;
    
    public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getPvpUrl() {
		return pvpUrl;
	}

	public void setPvpUrl(String pvpUrl) {
		this.pvpUrl = pvpUrl;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Byte getEntryType() {
        return entryType;
    }

    public void setEntryType(Byte entryType) {
        this.entryType = entryType;
    }

    public BigDecimal getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(BigDecimal entryFee) {
        this.entryFee = entryFee;
    }

    public String getEntryProductId() {
        return entryProductId;
    }

    public void setEntryProductId(String entryProductId) {
        this.entryProductId = entryProductId;
    }

    public Byte getRewardType() {
        return rewardType;
    }

    public void setRewardType(Byte rewardType) {
        this.rewardType = rewardType;
    }

    public BigDecimal getReward() {
        return reward;
    }

    public void setReward(BigDecimal reward) {
        this.reward = reward;
    }

    public Byte getPlatformBenefitType() {
        return platformBenefitType;
    }

    public void setPlatformBenefitType(Byte platformBenefitType) {
        this.platformBenefitType = platformBenefitType;
    }

    public BigDecimal getPlatformBenefit() {
        return platformBenefit;
    }

    public void setPlatformBenefit(BigDecimal platformBenefit) {
        this.platformBenefit = platformBenefit;
    }

    public Integer getCoinPoolSource() {
        return coinPoolSource;
    }

    public void setCoinPoolSource(Integer coinPoolSource) {
        this.coinPoolSource = coinPoolSource;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Date getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Integer getForder() {
        return forder;
    }

    public void setForder(Integer forder) {
        this.forder = forder;
    }

    public BigDecimal getRake() {
        return rake;
    }

    public void setRake(BigDecimal rake) {
        this.rake = rake;
    }

    public Byte getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(Byte roundCount) {
        this.roundCount = roundCount;
    }

    public Integer getDislodge() {
        return dislodge;
    }

    public void setDislodge(Integer dislodge) {
        this.dislodge = dislodge;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(Integer roundTime) {
        this.roundTime = roundTime;
    }

    public Integer getBenefitTimes() {
        return benefitTimes;
    }

    public void setBenefitTimes(Integer benefitTimes) {
        this.benefitTimes = benefitTimes;
    }

    public Byte getPvpType() {
        return pvpType;
    }

    public void setPvpType(Byte pvpType) {
        this.pvpType = pvpType;
    }

    public Boolean getBotSupport() {
        return botSupport;
    }

    public void setBotSupport(Boolean botSupport) {
        this.botSupport = botSupport;
    }

    public Integer getBotNumbers() {
        return botNumbers;
    }

    public void setBotNumbers(Integer botNumbers) {
        this.botNumbers = botNumbers;
    }
}