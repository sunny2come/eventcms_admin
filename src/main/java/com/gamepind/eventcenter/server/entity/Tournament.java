package com.gamepind.eventcenter.server.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Tournament implements Serializable {
	
    private Integer id;

    private String name;

    private Integer gameId;

    private Date startTime;

    private Date endTime;

    private Integer minPlayerCount;

    private Integer maxPlayerCount;

    private Byte entryType;

    private String entryProductId;

    private BigDecimal entryFee;

    private Byte rewardType;

    private BigDecimal maxReward;

    private Byte coinPoolSource;

    private Byte platformBenefitType;

    private BigDecimal platformBenefit;

    private BigDecimal rake;

    private Byte status;

    private Date createTime;

    private Date onlineTime;

    private Date offlineTime;

    private Integer forder;

    private Integer dislodge;

    private String channel;

    private String fileName;

    private Byte hasRoundTime;

    private Integer roundTime;
    private String pvpUrl;
    private String tournamentUrl;
    
	public String getPvpUrl() {
		return pvpUrl;
	}

	public void setPvpUrl(String pvpUrl) {
		this.pvpUrl = pvpUrl;
	}

	public String getTournamentUrl() {
		return tournamentUrl;
	}

	public void setTournamentUrl(String tournamentUrl) {
		this.tournamentUrl = tournamentUrl;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getMinPlayerCount() {
        return minPlayerCount;
    }

    public void setMinPlayerCount(Integer minPlayerCount) {
        this.minPlayerCount = minPlayerCount;
    }

    public Integer getMaxPlayerCount() {
        return maxPlayerCount;
    }

    public void setMaxPlayerCount(Integer maxPlayerCount) {
        this.maxPlayerCount = maxPlayerCount;
    }

    public Byte getEntryType() {
        return entryType;
    }

    public void setEntryType(Byte entryType) {
        this.entryType = entryType;
    }

    public String getEntryProductId() {
        return entryProductId;
    }

    public void setEntryProductId(String entryProductId) {
        this.entryProductId = entryProductId;
    }

    public BigDecimal getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(BigDecimal entryFee) {
        this.entryFee = entryFee;
    }

    public Byte getRewardType() {
        return rewardType;
    }

    public void setRewardType(Byte rewardType) {
        this.rewardType = rewardType;
    }

    public BigDecimal getMaxReward() {
        return maxReward;
    }

    public void setMaxReward(BigDecimal maxReward) {
        this.maxReward = maxReward;
    }

    public Byte getCoinPoolSource() {
        return coinPoolSource;
    }

    public void setCoinPoolSource(Byte coinPoolSource) {
        this.coinPoolSource = coinPoolSource;
    }

    public Byte getPlatformBenefitType() {
        return platformBenefitType;
    }

    public void setPlatformBenefitType(Byte platformBenefitType) {
        this.platformBenefitType = platformBenefitType;
    }

    public BigDecimal getPlatformBenefit() {
        return platformBenefit;
    }

    public void setPlatformBenefit(BigDecimal platformBenefit) {
        this.platformBenefit = platformBenefit;
    }

    public BigDecimal getRake() {
        return rake;
    }

    public void setRake(BigDecimal rake) {
        this.rake = rake;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Date getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Integer getForder() {
        return forder;
    }

    public void setForder(Integer forder) {
        this.forder = forder;
    }

    public Integer getDislodge() {
        return dislodge;
    }

    public void setDislodge(Integer dislodge) {
        this.dislodge = dislodge;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Byte getHasRoundTime() {
        return hasRoundTime;
    }

    public void setHasRoundTime(Byte hasRoundTime) {
        this.hasRoundTime = hasRoundTime;
    }

    public Integer getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(Integer roundTime) {
        this.roundTime = roundTime;
    }
}