package com.heeexy.example.util.excel;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

public class ExcelUtils {

	/**
	 * 传入一个注解了@CellMapping类的class对象，获取
	 * 标记了该注解的值和set方法名
	 * @return Map<cellName,methodName>
	 * */
	public static Map<String,String> getAnnotations(Class clazz){
		Map<String,String> map=new HashMap<String,String>();
		Field[] fields = clazz.getDeclaredFields();
		for(Field field:fields) {
			String fieldName = field.getName();
			CellMapping cell = field.getAnnotation(CellMapping.class);
			if(cell!=null) {
				String cellName = cell.cellName();
				String methodName="set"+StringUtils.capitalize(fieldName);
				map.put(cellName, methodName);
			}
		}
		return map;
	}
	
}
