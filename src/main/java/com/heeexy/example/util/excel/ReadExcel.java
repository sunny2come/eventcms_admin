package com.heeexy.example.util.excel;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbookFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 1、遍历每一个sheet，首先取到首行，然后将首行各个标题和所属列的索引数字组成Map<String,int>，然后开始遍历整个sheet的每一行
 2、先Station获取到方法名和对应的注解值(即excel中的标题名)，然后就知道具体的方法名对应的是当前行哪个列的值，此时可以通过反射来封装station对象了
 3、封装完station，拿到一个List<Station>，通过lambda转换成Map<电站名字，电站ID>以及Map<电站ID，电站名字>
 4、之后遍历整个List<Station>，为每个Station将前后电站的id复制进去
 5、将List<Station>存入数据库
 
 泛型的类型参数只能是类类型（包括自定义类），不能是简单类型
 在实例化泛型类时，必须指定T的具体类型ReadExcel<Station>
 */
public class ReadExcel<T> {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**指定一个Excel标题String notBlankField 如果当前行这列为空，就代表此行无效*/
	public List<T> importExcel(MultipartFile file,Class<T> t,String notBlankField) {
		List<T> resultList=new ArrayList<T>();
		//Map<cellName,methodName>
		Map<String, String> mapping = ExcelUtils.getAnnotations(t);
		Workbook workbook=null;
		try {
			workbook = HSSFWorkbookFactory.create(file.getInputStream());
		} catch (EncryptedDocumentException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		if(workbook==null) {
			return null;
		}
		Iterator<Sheet> sheets = workbook.iterator();
		while(sheets.hasNext()) {
			//Map<cellName,index>  列标题名和其索引
			Map<String,Integer> map=new HashMap<String,Integer>();
			Sheet sheet = sheets.next();
			//获取首行标题
			Row titleRow = sheet.getRow(0);
			int lastCellNum = titleRow.getLastCellNum()+1;
			for(int i = 0; i < lastCellNum; i++) {
				Cell cell = titleRow.getCell(i);
				if(cell==null) {
					continue;
				}
				String stringCellValue = cell.getStringCellValue();
				if(StringUtils.isNotBlank(stringCellValue)) {
					map.put(stringCellValue, i);
				}
			}
			
			//开始遍历数据行
			int rowNum = sheet.getLastRowNum()+1;
			for (int i = 1; i < rowNum; i++) {
				T newInstance=null;
				try {
					newInstance = t.newInstance();
				} catch (InstantiationException | IllegalAccessException e1) {
					System.out.println(e1);
				}
				Row dataRow = sheet.getRow(i);
				if(dataRow==null) {
					break;
				}else {
					Integer notBlankIndex = map.get(notBlankField);
					Cell ifcell = dataRow.getCell(notBlankIndex);
					if(ifcell==null || ifcell.getCellType() ==CellType.BLANK ) {
						continue;
					}
				}
				Iterator<Entry<String, String>> mapIterator = mapping.entrySet().iterator();
				while(mapIterator.hasNext()) {
					Entry<String, String> entry = mapIterator.next();
					String cellName = entry.getKey();
					Integer integer = map.get(cellName);
					if(integer==null) {
						System.out.println("Excel标题跟实体bean的注解值没对应上，字段为"+cellName);
					}
					Cell cell = dataRow.getCell(integer);
					if(cell==null) {
						continue;
					}
					String methodName = entry.getValue();
					try {
					switch (cell.getCellType()) {
					case NUMERIC:
						double numericCellValue = cell.getNumericCellValue();
						int num = (new Double(numericCellValue)).intValue();
						Method numericMethod = t.getMethod(methodName,Integer.class);
						numericMethod.invoke(newInstance,num);
						break;
					case STRING:
						String stringCellValue = cell.getStringCellValue();
						if("是".equals(stringCellValue)) {
							Method flageMethod = t.getMethod(methodName,Integer.class);
							flageMethod.invoke(newInstance,1);
						}else if("否".equals(stringCellValue)){
							Method flageMethod = t.getMethod(methodName,Integer.class);
							flageMethod.invoke(newInstance,0);
						}else {
							Method stringMethod = t.getMethod(methodName, String.class);
							stringMethod.invoke(newInstance, stringCellValue);
						}
						break;
					case FORMULA:
						double formulaValue = cell.getNumericCellValue();
						BigDecimal formulaDecimal=new BigDecimal(formulaValue);
						Method formulaMethod = t.getMethod(methodName,BigDecimal.class);
						formulaMethod.invoke(newInstance, formulaDecimal);
						break;
					case BOOLEAN:
						boolean booleanCellValue = cell.getBooleanCellValue();
						Method booleanMethod = t.getMethod(methodName,Boolean.class);
						booleanMethod.invoke(newInstance, booleanCellValue);
						break;
					}
					}catch(Exception e) {
						System.out.println("ReadExcel->importExcel反射执行set方法出错"+e);
					}
					
				}
				Method[] methods = t.getMethods();
				for(Method method : methods) {
					if("setId".equals(method.getName())) {
						 Class<?>[] parameterTypes = method.getParameterTypes();
						  String name = parameterTypes[0].getName();
						if("java.lang.String".equals(name)) {
							try {
								 String randomUUID = UUID.randomUUID().toString();
								String replace = randomUUID.replace("-", "");
								method.invoke(newInstance,replace);
							}catch(Exception e) {
								System.out.println("ReadExcel->importExcel反射执行set方法出错"+e);
							}
						}
					}
				}
				
				resultList.add(newInstance);
			}
		}
		
		return resultList;
	}
	
	public String getRandomString(int length) {
		// 定义一个字符串（A-Z，a-z，0-9）即62位；
		String str = "zxcvbnmlkjhgfdsaqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
		// 由Random生成随机数
		SecureRandom random = new SecureRandom();
		StringBuffer sb = new StringBuffer();
		// 长度为几就循环几次
		for (int i = 0; i < length; ++i) {
			// 产生0-61的数字
			int number = random.nextInt(62);
			// 将产生的数字通过length次承载到sb中
			sb.append(str.charAt(number));
		}
		// 将承载的字符转换成字符串
		return sb.toString();
	}
}
