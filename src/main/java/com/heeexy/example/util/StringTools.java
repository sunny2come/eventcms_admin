package com.heeexy.example.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: hxy
 * @date: 2017/10/24 10:16
 */
public class StringTools {

	public static boolean isNullOrEmpty(String str) {
		return null == str || "".equals(str) || "null".equals(str);
	}
	public static boolean isNullOrEmptyOrNegative(String str) {
		return null == str || "".equals(str) || "null".equals(str)||"-1".equals(str);
	}
	public static boolean isNullOrEmpty(Object obj) {
		return null == obj || "".equals(obj);
	}

	public static Date changeDate(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Long time = new Long(date);

			String d = format.format(time);
			return format.parse(d);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return null;
	}
}
