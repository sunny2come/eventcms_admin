package com.heeexy.example.util.constants;

/**
 * @author: hxy
 * @date: 2017/10/24 10:16
 */
public enum ErrorEnum {
	/*
	 * 错误信息
	 */
//	E_400("400", "请求处理异常，请稍后再试"),
//	E_500("500", "请求方式有误,请检查 GET/POST"),
//	E_501("501", "请求路径不存在"),
//	E_502("502", "权限不足"),
//	E_10008("10008", "角色删除失败,尚有用户属于此角色"),
//	E_10009("10009", "账户已存在"),
//
//	E_20011("20011", "登陆已过期,请重新登陆"),
//
//	E_90003("90003", "缺少必填参数"),
//	E_90004("90004", "没有对应的在线Event,该游戏无法上线"),
//	E_90005("90005", "该游戏还有对应Event没下线,无法下线");
	E_400("400", "Request processing exception, please try again later"),
	E_500("500", "The request method is incorrect, please check GET/POST"), E_501("501", "Request path does not exist"),
	E_502("502", "Insufficient permissions"),
	E_10008("10008", "The role deletion failed, there are still users belonging to this role"),
	E_10009("10009", "Account already exists"),

	E_20011("20011", "Login has expired, please log in again"),

	E_90003("90003", "Missing required parameters"),
	E_90004("90004", "There has no online event, game can't be online"),
	E_90005("90005", "The game still has online events, game can't go offline"),
	E_90006("90006", "The Excel data is error"), 
	E_88888("90007", "clone failed");

	private String errorCode;

	private String errorMsg;

	ErrorEnum(String errorCode, String errorMsg) {
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

}