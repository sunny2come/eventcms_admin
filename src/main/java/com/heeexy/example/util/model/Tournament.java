package com.heeexy.example.util.model;

import java.math.BigDecimal;
import java.util.Date;
/**
 * Tournament 和 PVP Event共用类
 * @author jonny
 *
 */
public class Tournament {
	private Integer id;

	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String eventName;
	private String gameId;
	private String entryType;
	private String rewardType;
	private Integer coinPoolSource;
	private String channel;
	private Date startTime;
	private int roundTime;	
	private Date createTime;
	private String  entryProductId;
	private Integer forder;
	
	private Date endTime;
	private String minimumPlayers;
	private String maximumPlayers;

	private BigDecimal benefit;
	private BigDecimal rake;
	
	private int hasRoundTime;
	
	private Byte platformBenefitType;
	private BigDecimal maxReward;
	
	
	public Integer getForder() {
		return forder;
	}

	public void setForder(Integer forder) {
		this.forder = forder;
	}

	public Byte getPlatformBenefitType() {
		return platformBenefitType;
	}

	public void setPlatformBenefitType(Byte platformBenefitType) {
		this.platformBenefitType = platformBenefitType;
	}

	public BigDecimal getMaxReward() {
		return maxReward;
	}

	public void setMaxReward(BigDecimal maxReward) {
		this.maxReward = maxReward;
	}

	public String getEntryProductId() {
		return entryProductId;
	}

	public void setEntryProductId(String entryProductId) {
		this.entryProductId = entryProductId;
	}

	public int getHasRoundTime() {
		return hasRoundTime;
	}

	public void setHasRoundTime(int hasRoundTime) {
		this.hasRoundTime = hasRoundTime;
	}

	/**
	 *  It is only 
	 */
	private String fileName;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * PVP 单独有
	 */
	private String benefitTimes;
	private String roundCount;
	private BigDecimal reward;
	private Integer pvpType;
	private Integer botSupport;
	private Integer botNumbers;
	
	public Integer getPvpType() {
		return pvpType;
	}

	public void setPvpType(Integer pvpType) {
		this.pvpType = pvpType;
	}

	public Integer getBotSupport() {
		return botSupport;
	}

	public void setBotSupport(Integer botSupport) {
		this.botSupport = botSupport;
	}

	public Integer getBotNumbers() {
		return botNumbers;
	}

	public void setBotNumbers(Integer botNumbers) {
		this.botNumbers = botNumbers;
	}

	public BigDecimal getReward() {
		return reward;
	}

	public void setReward(BigDecimal reward) {
		this.reward = reward;
	}

	public String getBenefitTimes() {
		return benefitTimes;
	}

	public void setBenefitTimes(String benefitTimes) {
		this.benefitTimes = benefitTimes;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getRoundTime() {
		return roundTime;
	}

	public void setRoundTime(int roundTime) {
		this.roundTime = roundTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	
	
	public BigDecimal getBenefit() {
		return benefit;
	}

	public void setBenefit(BigDecimal benefit) {
		this.benefit = benefit;
	}

	public BigDecimal getRake() {
		return rake;
	}

	public void setRake(BigDecimal rake) {
		this.rake = rake;
	}

	private BigDecimal entryFee;

	public BigDecimal getEntryFee() {
		return entryFee;
	}

	public void setEntryFee(BigDecimal entryFee) {
		this.entryFee = entryFee;
	}



	// #{benefit},
	// #{rake},
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getEntryType() {
		return entryType;
	}

	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}

	public String getRewardType() {
		return rewardType;
	}

	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public String getChannel() {
		return channel;
	}

	public Integer getCoinPoolSource() {
		return coinPoolSource;
	}

	public void setCoinPoolSource(Integer coinPoolSource) {
		this.coinPoolSource = coinPoolSource;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMinimumPlayers() {
		return minimumPlayers;
	}

	public void setMinimumPlayers(String minimumPlayers) {
		this.minimumPlayers = minimumPlayers;
	}

	public String getMaximumPlayers() {
		return maximumPlayers;
	}

	public void setMaximumPlayers(String maximumPlayers) {
		this.maximumPlayers = maximumPlayers;
	}

	public String getRoundCount() {
		return roundCount;
	}

	public void setRoundCount(String roundCount) {
		this.roundCount = roundCount;
	}

}
