package com.heeexy.example.util.model.vo;

import com.heeexy.example.util.model.BotScoreRange;

public class BotScoreRangeVO extends BotScoreRange {

	private String name;//gameName

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
