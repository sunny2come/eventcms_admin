package com.heeexy.example.util.model;

import com.heeexy.example.util.excel.CellMapping;

public class BotScoreRange {
    public Integer id;

    public Integer gameId;

    @CellMapping(cellName="MaxScore")
    public Integer maxScore;

    @CellMapping(cellName="MinScore")
    public Integer minScore;

    @CellMapping(cellName="Second")
    public Integer second;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Integer maxScore) {
        this.maxScore = maxScore;
    }

    public Integer getMinScore() {
        return minScore;
    }

    public void setMinScore(Integer minScore) {
        this.minScore = minScore;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }
}