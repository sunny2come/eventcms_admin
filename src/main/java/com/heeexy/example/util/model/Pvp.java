package com.heeexy.example.util.model;

import java.math.BigDecimal;
import java.util.Date;

public class Pvp {
    private Integer id;

    private String name;

    private Integer gameId;

    private Date startTime;

    private Date endTime;

    private Byte entryType;

    private Byte rewardType;

    private BigDecimal entryFee;

    private BigDecimal platformBenefit;

    private Integer coinPoolType;

    private Byte status;

    private Date createTime;

    private Date onlineTime;

    private Date offlineTime;

    private Integer forder;

    private BigDecimal rake;

    private Byte rewardRule;

    private Integer dislodge;

    private String channel;

    private String rule;

    private String zoneApiKey;

    private Integer roundTime;

    private Integer benefitTimes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Byte getEntryType() {
        return entryType;
    }

    public void setEntryType(Byte entryType) {
        this.entryType = entryType;
    }

    public Byte getRewardType() {
        return rewardType;
    }

    public void setRewardType(Byte rewardType) {
        this.rewardType = rewardType;
    }

    public BigDecimal getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(BigDecimal entryFee) {
        this.entryFee = entryFee;
    }

    public BigDecimal getPlatformBenefit() {
        return platformBenefit;
    }

    public void setPlatformBenefit(BigDecimal platformBenefit) {
        this.platformBenefit = platformBenefit;
    }

    public Integer getCoinPoolType() {
        return coinPoolType;
    }

    public void setCoinPoolType(Integer coinPoolType) {
        this.coinPoolType = coinPoolType;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Date getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Integer getForder() {
        return forder;
    }

    public void setForder(Integer forder) {
        this.forder = forder;
    }

    public BigDecimal getRake() {
        return rake;
    }

    public void setRake(BigDecimal rake) {
        this.rake = rake;
    }

    public Byte getRewardRule() {
        return rewardRule;
    }

    public void setRewardRule(Byte rewardRule) {
        this.rewardRule = rewardRule;
    }

    public Integer getDislodge() {
        return dislodge;
    }

    public void setDislodge(Integer dislodge) {
        this.dislodge = dislodge;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getZoneApiKey() {
        return zoneApiKey;
    }

    public void setZoneApiKey(String zoneApiKey) {
        this.zoneApiKey = zoneApiKey;
    }

    public Integer getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(Integer roundTime) {
        this.roundTime = roundTime;
    }

    public Integer getBenefitTimes() {
        return benefitTimes;
    }

    public void setBenefitTimes(Integer benefitTimes) {
        this.benefitTimes = benefitTimes;
    }
}