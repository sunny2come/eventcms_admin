package com.heeexy.example.util.model;

import com.heeexy.example.util.excel.CellMapping;

public class Bot {
	
	 @CellMapping(cellName="bot_id")
    private String botId;

	 @CellMapping(cellName="bot_avatar")
    private String avatar;

    @CellMapping(cellName="bot_nickname")
    private String nickName;

    @CellMapping(cellName="bot_level")
    private Integer botLevel;

    public String getBotId() {
        return botId;
    }

    public void setBotId(String botId) {
        this.botId = botId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getBotLevel() {
        return botLevel;
    }

    public void setBotLevel(Integer botLevel) {
        this.botLevel = botLevel;
    }

}