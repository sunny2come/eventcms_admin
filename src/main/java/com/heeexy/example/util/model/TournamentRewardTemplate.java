package com.heeexy.example.util.model;

import com.heeexy.example.util.ExcelUtil.ExcelCell;
import com.heeexy.example.util.excel.CellMapping;

public class TournamentRewardTemplate {
	
	
    private Integer id;

    private Integer touramentId;
    
    @CellMapping(cellName="Winner Rank")
//    @ExcelCell(index = 0)
    private Integer rank;
    @CellMapping(cellName="No. of winners")
//    @ExcelCell(index = 1)

    private Integer numofwin;
    @CellMapping(cellName="Prize percentage")
//    @ExcelCell(index =2 )

    private String prizePercentage;
    @CellMapping(cellName="Total prize percentage")
//    @ExcelCell(index = 3)

    private String totalPrizePercentage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTouramentId() {
        return touramentId;
    }

    public void setTouramentId(Integer touramentId) {
        this.touramentId = touramentId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getNumofwin() {
        return numofwin;
    }

    public void setNumofwin(Integer numofwin) {
        this.numofwin = numofwin;
    }

    public String getPrizePercentage() {
        return prizePercentage;
    }

    public void setPrizePercentage(String prizePercentage) {
        this.prizePercentage = prizePercentage;
    }

    public String getTotalPrizePercentage() {
        return totalPrizePercentage;
    }

    public void setTotalPrizePercentage(String totalPrizePercentage) {
        this.totalPrizePercentage = totalPrizePercentage;
    }
}