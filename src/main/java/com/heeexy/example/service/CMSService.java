package com.heeexy.example.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.heeexy.example.util.model.Bot;
import com.heeexy.example.util.model.BotScoreRange;
import com.heeexy.example.util.model.Game;
import com.heeexy.example.util.model.vo.BotScoreRangeVO;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
public interface CMSService {

	/**
	 * 游戏列表
	 */
	JSONObject listGame(JSONObject jsonObject);
	/**
	 * 游戏对应Tournament列表
	 */
	JSONObject listTournament(JSONObject jsonObject);
	/**
	 * 游戏对应PVP列表
	 */
	JSONObject listPVP(JSONObject jsonObject);

	/**
	 * 更新游戏状态
	 */
	JSONObject gameOffline(Integer gameId,Integer business);

	List<JSONObject> selectProductIdByType(Integer type);

	List<Game> listAllGame();

	List<BotScoreRangeVO> listBotGameRange(Integer gameId);

	boolean batchInsertScore(List<BotScoreRange> tempScoreList);

	void batchInsertBot(List<Bot> list);

	List<Bot> allBotList();

	void truncateData(String tableName);

	boolean insertGame(Game game);

	boolean updateGame(Game game);

	PageInfo<JSONObject> listEvents(Integer gameId,Integer pageNum,Integer pageRow);

	boolean updateEventNumber(JSONObject requestJson);

	JSONObject listGameForder(JSONObject jsonObject);

	boolean updateGameForder(JSONObject requestJson);
}
