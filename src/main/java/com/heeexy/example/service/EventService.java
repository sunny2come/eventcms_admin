package com.heeexy.example.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author: hxy
 * @date: 2017/10/24 16:06
 */
public interface EventService {
	/**
	 * 新增文章
	 */
	JSONObject addEvent(JSONObject jsonObject);

	/**
	 * 文章列表
	 */
	JSONObject listEvent(JSONObject jsonObject);

	/**
	 * 更新文章
	 */
	JSONObject updateArticle(JSONObject jsonObject);
}
