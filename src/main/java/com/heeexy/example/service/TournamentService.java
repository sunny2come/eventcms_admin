package com.heeexy.example.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.util.model.Tournament;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
public interface TournamentService {

	/**
	 * 游戏列表
	 */
	JSONObject listGame(JSONObject jsonObject);

	/**
	 * Tournament列表
	 */
	JSONObject listTournament(JSONObject jsonObject);

	/**
	 * 新增Tournament
	 */
	JSONObject addTournament(JSONObject jsonObject);

	/**
	 * 上线
	 */
	JSONObject updateStatus(JSONObject jsonObject);

	Tournament selectByTid(Integer tid);

	JSONObject clone(Integer integer);

}
