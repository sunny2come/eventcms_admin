package com.heeexy.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.heeexy.example.controller.Task;
import com.heeexy.example.feign.RemoteServer;
import com.heeexy.example.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService{

	@Autowired
	RemoteServer remoteServer;

	@Override
	public List<Task> getTaskList() {
		return remoteServer.getTaskList();
	}

	@Override
	public void cacheTask() {
		remoteServer.cacheTask();
	}

	@Override
	public void loadTask() {
		remoteServer.loadTask();
	}

	@Override
	public void createTask(String type, int sourceId, String date) {
		remoteServer.createTask(Integer.parseInt(type), sourceId, date);
	}

	@Override
	public void cancelTask(String type, Integer sourceId) {
		remoteServer.cancelTask(Integer.parseInt(type), sourceId);
	}
	
	
}
