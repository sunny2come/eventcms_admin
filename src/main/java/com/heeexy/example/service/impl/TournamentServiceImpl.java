package com.heeexy.example.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.dao.ArticleDao;
import com.heeexy.example.dao.CMSDao;
import com.heeexy.example.dao.PVPDao;
import com.heeexy.example.dao.TournamentDao;
import com.heeexy.example.feign.RemoteServer;
import com.heeexy.example.service.ArticleService;
import com.heeexy.example.service.CMSService;
import com.heeexy.example.service.TournamentService;
import com.heeexy.example.util.CommonUtil;
import com.heeexy.example.util.StringTools;
import com.heeexy.example.util.ExcelUtil.ExcelHead;
import com.heeexy.example.util.ExcelUtil.ExcelLogs;
import com.heeexy.example.util.ExcelUtil.ExcelUtil;
import com.heeexy.example.util.ExcelUtil.ExcelUtils;
import com.heeexy.example.util.constants.Constants;
import com.heeexy.example.util.constants.ErrorEnum;
import com.heeexy.example.util.excel.ReadExcel;
import com.heeexy.example.util.model.Reasearch;
import com.heeexy.example.util.model.Tournament;
import com.heeexy.example.util.model.TournamentRewardTemplate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
@Service
public class TournamentServiceImpl implements TournamentService {

	@Autowired
	private TournamentDao tDao;

	@Autowired
	private PVPDao pDao;
	
	@Autowired
	RemoteServer remoteServer;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	/**
	 * 上线
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject updateStatus(JSONObject jsonObject) {
		String status = jsonObject.getString("status");
		String id = jsonObject.getString("id");
		int tid = Integer.parseInt(id);
		if ("2".equals(status)) {
			Tournament t=tDao.selectById(tid);
			Date startTime = t.getStartTime();
			Date endTime = t.getEndTime();
			String one = dateFormat.format(startTime);
			String two = dateFormat.format(endTime);
			Map map = new HashMap();
			map.put("id",id);
			map.put("status", jsonObject.getString("status"));
			map.put("onLineTime", new Date());
			remoteServer.onlineTournament(tid,one,two);
			tDao.updateOnLine(map);
		} else if ("3".equals(status)) {
			Map map = new HashMap();
			map.put("id",id);
			map.put("status", jsonObject.getString("status"));
			map.put("offLineTime", new Date());
			remoteServer.offlineTournament(tid);
			tDao.updateOffLine(map);
		}
		return CommonUtil.successJson();
	}
	/**
	 * 上线
	 */
	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public JSONObject updateOffLine(JSONObject jsonObject) {
	// tDao.updateOffLine(jsonObject);
	// return CommonUtil.successJson();
	// }

	/**
	 * 
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject addTournament(JSONObject jsonObject) {

		// String 判断 jsonObject 必要字段
		System.out.println("addTournament requestJson" + jsonObject);

		String coinPoolSource = jsonObject.getString("coinPoolSource");
		Tournament t = new Tournament();
		t.setEntryProductId(jsonObject.getString("entryProductId"));
		t.setMaxReward(new BigDecimal(jsonObject.getString("maxReward")));
		t.setEventName(jsonObject.getString("eventName"));
		t.setGameId(jsonObject.getString("gameId"));
		String rewardType = jsonObject.getString("rewardType");

		if ("0".equals(rewardType) || !StringUtils.isNotEmpty(rewardType)) {// get ALl
		} else {
			t.setRewardType(rewardType);
			t.setPlatformBenefitType(Byte.valueOf(rewardType));
		}

		t.setCoinPoolSource(Integer.parseInt(coinPoolSource));
		try {
			t.setEntryFee(new BigDecimal(jsonObject.getString("entryFee")));
		} catch (Exception e) {
		}
		t.setChannel(jsonObject.getString("channel"));
		Long time = new Long(jsonObject.getString("startTime"));
		Long end = new Long(jsonObject.getString("endTime"));

			Date date = new Date(time);
			Date endDate = new Date(end);
			t.setStartTime(date);
			t.setEndTime(endDate);

		t.setCreateTime(new Date());

		// t.setEndTime(new Date(jsonObject.getString("endTime")));
		t.setMinimumPlayers(jsonObject.getString("minimumPlayers"));
		t.setMaximumPlayers(jsonObject.getString("maximumPlayers"));
		t.setRoundCount(jsonObject.getString("rule"));
		// todo
		t.setEntryType(jsonObject.getString("entryType"));
		System.out.println("requestJson-----list" + jsonObject);
		try {
			int isHas = jsonObject.getIntValue("hasRoundTime");
			if(0==isHas) {// 没有
				t.setRoundTime(0);
				t.setHasRoundTime(0);
			}else {
				t.setRoundTime(jsonObject.getIntValue("roundTime"));
				t.setHasRoundTime(1);
			}
		} catch (Exception e) {
		}

		
		double excelRake = 100.00;
		if (!StringTools.isNullOrEmpty(coinPoolSource)) {
			if (coinPoolSource.equals("1")) {// from platform 
				jsonObject.put("rake", "0");
				// jsonObject.put("entryFee", "0");
				// t.setEntryFee(new BigDecimal(0));
				t.setRake(new BigDecimal("0"));
				String benefit = jsonObject.getString("maxReward");

				if (StringUtils.isNotEmpty(benefit)) {
					try {
						t.setBenefit(new BigDecimal(benefit));
					} catch (Exception e) {

					}
				}
				// jsonObject.put("benefit", benefit);

			} else if (coinPoolSource.equals("2")) {// from player 
				double rake = jsonObject.getDouble("rake");
				// jsonObject.put("rake", );
				excelRake -= rake;
				t.setRake(new BigDecimal(rake / 100));

				String benefit = jsonObject.getString("benefit");

				if (StringUtils.isNotEmpty(benefit)) {
					try {
						t.setBenefit(new BigDecimal(benefit));
					} catch (Exception e) {

					}
				}

				t.setRake(new BigDecimal(rake / 100));
			}
		}
		System.out.println("requestJson-----list2" + jsonObject);
		// jsonObject.put("benefit", new BigDecimal(2));
		String id = jsonObject.getString("id");
		t.setId(Integer.parseInt(id));

		int tournamentId = 0;
		if ("0".equals(id)) {
			String fileName = jsonObject.getString("fileName");

			t.setFileName(fileName);
			if (checkExcel(fileName, excelRake)) {
				if(t.getBenefit()==null) {
					t.setBenefit(BigDecimal.ZERO);
				}
				int maxForder = pDao.selectMaxForder();
				t.setForder(maxForder+1);
				tDao.addTournament(t);
				tournamentId = t.getId();
				System.out.println("requestJson-----tournamentId" + tournamentId);
				// TODO 以后扩展按时期
				insertExcel(fileName, tournamentId);
			} else {
				System.out.println("requestJson-----ErrorEnum.E_90006" + ErrorEnum.E_90006);

				return CommonUtil.errorJson(ErrorEnum.E_90006);
			}
		} else {
			String name = tDao.getFileNameById(jsonObject);
			String fileName = jsonObject.getString("fileName");
			if (fileName!=null && fileName.equals(name)) {
				tDao.updateTournament(t);
			} else {// 文件有修改
				t.setFileName(fileName);
				if (checkExcel(fileName, excelRake)) {
					tDao.deleteTournamentRewardTemplateById(t.getId());
					// 重新插入 excel
					insertExcel(fileName, t.getId());
					tDao.updateTournament(t);
				} else {
					return CommonUtil.errorJson(ErrorEnum.E_90006);
				}
			}
		}
		return CommonUtil.successJson();
	}

	private boolean checkExcel(String fileName, Double rake) {
		File file = new File("excel/" + fileName);
		FileInputStream in = null;
			try {
				in = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				return false;
			}

			List<ExcelHead> excelHeads = new ArrayList<ExcelHead>();
			ExcelHead excelHead = new ExcelHead("Winner Rank", "rank");
			ExcelHead excelHead1 = new ExcelHead("No. of winners", "numofwin");
			ExcelHead excelHead2 = new ExcelHead("Prize percentage(%)", "prizePercentage");
			ExcelHead excelHead3 = new ExcelHead("Total prize percentage(%)", "totalPrizePercentage");
			// ExcelHead excelHead3 = new ExcelHead("Total prize percentage", "address",
			// true);
			excelHeads.add(excelHead);
			excelHeads.add(excelHead1);
			excelHeads.add(excelHead2);
			excelHeads.add(excelHead3);
			List<TournamentRewardTemplate> list = null;
			try {
				list = ExcelUtils.readExcelToEntity(TournamentRewardTemplate.class, in,
						file.getName(), excelHeads);
			} catch (Exception e) {
				return false;
			}
			BigDecimal pAll = BigDecimal.ZERO;
			BigDecimal toAll = BigDecimal.ZERO;
			BigDecimal lastOne = BigDecimal.ZERO;

			int index = 0;
			for (TournamentRewardTemplate item : list) {
				item.setNumofwin(1);
				if (!StringTools.isNullOrEmpty(item.getPrizePercentage())) {

					if(0==index) {
						lastOne = new BigDecimal(item.getPrizePercentage());
						System.out.println("requestJson-----lastOne---" + lastOne);
					}else {
					}
					index++;
					BigDecimal a=new BigDecimal(item.getPrizePercentage());
					pAll=pAll.add(a);
				}
				// item.setTouramentId(new Integer(tournamentId));
			}
			System.out.println("requestJson-----rake---" + rake);

			System.out.println("pAll------" + pAll);
			System.out.println("toAll------" + toAll);
			BigDecimal d=new BigDecimal(100);
			
			if (d.compareTo(pAll)==0) {
				return true;
			} else {
				return false;
			}
	}

	private void insertExcel(String fileName, int tournamentId) {
		File file = new File("excel/" + fileName);
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);

			List<ExcelHead> excelHeads = new ArrayList<ExcelHead>();
			ExcelHead excelHead = new ExcelHead("Winner Rank", "rank");
			ExcelHead excelHead1 = new ExcelHead("No. of winners", "numofwin");
			ExcelHead excelHead2 = new ExcelHead("Prize percentage(%)", "prizePercentage");
			ExcelHead excelHead3 = new ExcelHead("Total prize percentage(%)", "totalPrizePercentage");
			// ExcelHead excelHead3 = new ExcelHead("Total prize percentage", "address",
			// true);
			excelHeads.add(excelHead);
			excelHeads.add(excelHead1);
			excelHeads.add(excelHead2);
			excelHeads.add(excelHead3);
			List<TournamentRewardTemplate> list = ExcelUtils.readExcelToEntity(TournamentRewardTemplate.class, in,
					file.getName(), excelHeads);
			for (TournamentRewardTemplate item : list) {
				if (!StringTools.isNullOrEmpty(item.getPrizePercentage())) {
					item.setTouramentId(new Integer(tournamentId));
					item.setNumofwin(1);
					item.setTotalPrizePercentage(item.getPrizePercentage());
					tDao.addTournamentRewardTemplate(item);
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 游戏列表
	 */
	@Override
	public JSONObject listGame(JSONObject jsonObject) {
		// CommonUtil.fillPageParam(jsonObject);
		// int count = tDao.countTournament(jsonObject);
		// List<JSONObject> list = tDao.listGame(jsonObject);
		// return CommonUtil.successPage(jsonObject, list, count);
		List<JSONObject> list = tDao.listTournament(null);
		return CommonUtil.successPage(list);
	}

	/**
	 * Tournament列表
	 */
	@Override
	public JSONObject listTournament(JSONObject jsonObject) {
		CommonUtil.fillPageParam(jsonObject);
		// int count = cmsDao.countGame(jsonObject);
		String status = jsonObject.getString("status");
		String entryType = jsonObject.getString("entryType");
		Date startTime = StringTools.changeDate(jsonObject.getString("startTime"));
		Date endTime = StringTools.changeDate(jsonObject.getString("endTime"));
		String name = jsonObject.getString("name");
		String gameId = jsonObject.getString("gameId");
		String channel = jsonObject.getString("channel");

		String offSet = jsonObject.getString("offSet");
		String pageRow = jsonObject.getString("pageRow");
		System.out.println("requestJson-----list" + jsonObject);

		Reasearch r = new Reasearch();
		// 为 -1 和 空的时候 查询全部
		// entryType
		// participants
		// channel
		// coinPool
		if (!StringTools.isNullOrEmpty(status)) {
			r.setStatus(status);
		}
		if (!StringTools.isNullOrEmptyOrNegative(entryType)) {
			r.setEntryType(entryType);
		}
		if (startTime != null) {
			r.setStartTime(startTime);
		}
		if (endTime != null) {
			r.setEndTime(endTime);
			;
		}
		if (!StringTools.isNullOrEmpty(name)) {
			r.setName(name);
		}
		if (!StringTools.isNullOrEmptyOrNegative(gameId)) {
			r.setGameId(gameId);
		}
		if (!StringTools.isNullOrEmptyOrNegative(channel)) {
			r.setChannel(channel);
		}
		if (!StringTools.isNullOrEmpty(offSet)) {
			r.setOffSet(Integer.parseInt(offSet));
		}
		if (!StringTools.isNullOrEmpty(pageRow)) {
			r.setPageRow(Integer.parseInt(pageRow));
		}

		String participants = jsonObject.getString("participants");
		String coinPool = jsonObject.getString("coinPool");
		String orderBy;

		String orderByString = " ";
		if (!StringTools.isNullOrEmptyOrNegative(participants)) {
			// if(participants.contains("asc")) {
			// r.setAsc("asc");
			// }
			// if(participants.contains("desc")) {
			// r.setDesc("asc");
			// }
			orderByString += " order by tr.participants " + participants;
			if (!StringTools.isNullOrEmptyOrNegative(coinPool)) {
				orderByString += " , tr.coinPool " + coinPool;
			}
		} else {
			if (!StringTools.isNullOrEmptyOrNegative(coinPool)) {
				orderByString += " order by tr.coinPool " + coinPool;
			} else {// 没有排序需求,清空
				orderByString = " order by t.create_time desc";
			}
		}
		System.out.println("requestJson-----orderByString" + orderByString);

		if (!StringTools.isNullOrEmpty(orderByString)) {
			// jsonObject.put("orderBy", orderByString);
			r.setOrderBy(orderByString + " ");
		}
		System.out.println("requestJson-----setOrderBy" + r.getOrderBy() + "==");

		int count = tDao.countTournament(r);
		// System.out.println("requestJson-----list"+r.getOffSet());
		// System.out.println("requestJson-----list"+r.getPageRow());
		// r.setStatus("1");
		List<JSONObject> list = tDao.listTournament(r);
		System.out.println("requestJson-----list" + list.size());

		return CommonUtil.successPage(jsonObject, list, count);
	}

	@Override
	public Tournament selectByTid(Integer tid) {
		return tDao.selectById(tid);
	}

	@Override
	public JSONObject clone(Integer tid) {
		try {
			tDao.cloneTournament(tid);
			return CommonUtil.successJson();
		}catch (Exception e) {
			return CommonUtil.errorJson(ErrorEnum.E_88888);
		}
	}

	/**
	 * 更新游戏状态
	 */
	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public JSONObject updateGameStatus(JSONObject jsonObject) {
	// tDao.updateGameStatus(jsonObject);
	// return CommonUtil.successJson();
	// }
}
