package com.heeexy.example.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gamepind.eventcenter.server.ddo.TournamentDo;
import com.gamepind.eventcenter.server.entity.Pvp;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.heeexy.example.dao.CMSDao;
import com.heeexy.example.service.CMSService;
import com.heeexy.example.util.CommonUtil;
import com.heeexy.example.util.model.Bot;
import com.heeexy.example.util.model.BotScoreRange;
import com.heeexy.example.util.model.Game;
import com.heeexy.example.util.model.vo.BotScoreRangeVO;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
@Service
public class CMSServiceImpl implements CMSService {
	
	final String PREFIX_TOURNAMENT_ID = "TOURNAMENT_";
	final String PREFIX_PVP_ID = "PVP_";
	final String PREFIX_PVP_BY_GAME_ID = "GAME_PVP_";
	final String PREFIX_TOURNAMENT_BY_GAME_ID = "GAME_TOURNAMENT_";
	final String PREFIX_PVP_FRIEND = "PVP_FRIEND_";
	final String PREFIX_GAME_ = "PVP_GAME_";
	
	@Autowired
	private CMSDao cmsDao;
	
	@Autowired
	RedisTemplate redisTemplate;

	/**
	 * 游戏列表
	 */
	@Override
	public JSONObject listGame(JSONObject jsonObject) {
		CommonUtil.fillPageParam(jsonObject);
		int count = cmsDao.countGame(jsonObject);
		List<JSONObject> list = cmsDao.listGame(jsonObject);
		return CommonUtil.successPage(jsonObject, list, count);
	}
	
	@Override
	public JSONObject listGameForder(JSONObject jsonObject) {
		Integer pageNum = jsonObject.getInteger("pageNum");
		Integer pageRow = jsonObject.getInteger("pageRow");
		PageHelper.startPage(pageNum, pageRow);
		List<JSONObject> list = cmsDao.listGame(jsonObject);
		PageInfo<JSONObject> p = new PageInfo<JSONObject>(list);
		Long total = p.getTotal();
		return CommonUtil.successPage(jsonObject,p.getList(),total.intValue());
	}
	/**
	 * 游戏Tournament列表
	 */
	@Override
	public JSONObject listTournament(JSONObject jsonObject) {
		//CommonUtil.fillPageParam(jsonObject);
		//int count = cmsDao.countGame(jsonObject);
		List<JSONObject> list = cmsDao.listTournament(jsonObject);
		return CommonUtil.successPage(list);
	}
	/**
	 * 游戏Tournament列表
	 */
	@Override
	public JSONObject listPVP(JSONObject jsonObject) {
//		CommonUtil.fillPageParam(jsonObject);
		//int count = cmsDao.countGame(jsonObject);
		System.out.println("requestJson-----list1");
		List<JSONObject> list = cmsDao.listPVP(jsonObject);
		System.out.println("requestJson-----list"+list.size());
		return CommonUtil.successPage(list);
	}

	/**
	 * 更新游戏状态
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject gameOffline(Integer gameId,Integer business) {
		//business: 1是初次确认是否执行  2是确认执行
		switch (business) {
		case 1:
			//查询当前game下有多少pvp和tournament
			List<Integer> j=cmsDao.influenceEvent(gameId);
			return CommonUtil.successJson(j);
		case 2:
			cmsDao.gameOfflineById(gameId);
			deleteByKeyPrefix(PREFIX_GAME_);
			break;
		default:
			break;
		}
		return CommonUtil.successJson();
	}

	@Override
	public List<JSONObject> selectProductIdByType(Integer type) {
		return cmsDao.selectProductIdByType(type);
	}

	@Override
	public List<Game> listAllGame() {
		return cmsDao.listAllGame();
	}

	@Override
	public List<BotScoreRangeVO> listBotGameRange(Integer gameId) {
		return cmsDao.listBotGameRange(gameId);
	}

	@Override
	public boolean batchInsertScore(List<BotScoreRange> tempScoreList) {
		if(tempScoreList==null) {
			return false;
		}
		try {
			Integer gameId = tempScoreList.get(0).getGameId();
			cmsDao.deleteScores(gameId);
			cmsDao.batchInsertScore(tempScoreList);
			return true;
		}catch(Exception e) {
			return false;
		}
	}

	@Override
	public void batchInsertBot(List<Bot> list) {
		cmsDao.batchInsertBot(list);
	}

	@Override
	public List<Bot> allBotList() {
		return cmsDao.allBotList();
	}

	@Override
	public void truncateData(String tableName) {
		cmsDao.truncateData(tableName);
	}

	@Override
	public boolean insertGame(Game game) {
		game.setCreateTime(new Date());
		game.setForder(0);
		game.setStatus((byte)0);
		int code = cmsDao.insertGame(game);
		if(code==1) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public boolean updateGame(Game game) {
		int code = cmsDao.updateGame(game);
		if(code==1) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public PageInfo<JSONObject> listEvents(Integer gameId,Integer pageNum,Integer pageRow) {
		PageHelper.startPage(pageNum, pageRow);
		List<JSONObject> listEvent = cmsDao.listEvent(gameId);
		PageInfo<JSONObject> p = new PageInfo<JSONObject>(listEvent);
		return p;
	}

	@Override
	public boolean updateEventNumber(JSONObject requestJson) {
		Integer gameId = requestJson.getInteger("gameId");
		JSONArray jsonArray = requestJson.getJSONArray("data");
		List<JSONObject> pvpList=new ArrayList<>();
		List<JSONObject> tournamentList=new ArrayList<>();
		for(int i=0;i<jsonArray.size();i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			String event_type = jsonObject.getString("event_type");
			if("tournament".equals(event_type)) {
				tournamentList.add(jsonObject);
				Integer tid = jsonObject.getInteger("id");
				Integer forder = jsonObject.getInteger("forder");
				Integer oldforder = jsonObject.getInteger("oldforder");
				cacheTournament(tid, forder, gameId);
			}else if("pvp".equals(event_type)) {
				pvpList.add(jsonObject);
				Integer pid = jsonObject.getInteger("id");
				Integer forder = jsonObject.getInteger("forder");
				Integer oldforder = jsonObject.getInteger("oldforder");
				cachePvp(pid, forder, gameId);
			}
		}
		cmsDao.batchUpdateTnum(tournamentList);
		cmsDao.batchUpdatePnum(pvpList);
		deleteByKeyPrefix(PREFIX_PVP_FRIEND);
		Integer shamPlayTime = requestJson.getInteger("shamPlayTime");
		int code=cmsDao.updateShamPlayTime(gameId,shamPlayTime);
		return true;
	}

	@Override
	public boolean updateGameForder(JSONObject requestJson) {
		JSONArray jsonArray = requestJson.getJSONArray("data");
		cmsDao.batchUpdateGameForder(jsonArray);
		deleteByKeyPrefix(PREFIX_GAME_);
		return true;
	}
	
	public void cacheTournament(Integer id,Integer forder,Integer gameId){
		 try {
	            RedisConnectionUtils.bindConnection(redisTemplate.getConnectionFactory());
	            ValueOperations<String,TournamentDo> opsForValue = redisTemplate.opsForValue();
	            TournamentDo tournamentDo = opsForValue.get(PREFIX_TOURNAMENT_ID + id);
	            tournamentDo.setForder(forder);
	            opsForValue.set(PREFIX_TOURNAMENT_ID + id, tournamentDo);
	            ZSetOperations opsForZSet = redisTemplate.opsForZSet();
	            opsForZSet.add(PREFIX_TOURNAMENT_BY_GAME_ID + gameId,id,forder);
	        } finally {
	            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
	        }
	}
	public void cachePvp(Integer pvpId,Integer forder,Integer gameId){
		try {
			RedisConnectionUtils.bindConnection(redisTemplate.getConnectionFactory());

			ValueOperations<String, Pvp> opsForValue = redisTemplate.opsForValue();
			Pvp pvp = opsForValue.get(PREFIX_PVP_ID + pvpId);
			pvp.setForder(forder);
			opsForValue.set(PREFIX_PVP_ID + pvpId,pvp);
			ZSetOperations opsForZSet = redisTemplate.opsForZSet();
			opsForZSet.add(PREFIX_PVP_BY_GAME_ID+gameId,pvpId,forder);
		} finally {
			RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
		}
	}
	
	public void deleteByKeyPrefix(String keyPrefix) {
		try {
			RedisConnectionUtils.bindConnection(redisTemplate.getConnectionFactory());
			Set<String> keys = redisTemplate.keys(keyPrefix + "*");
			if (keys != null && keys.size() > 0) {
				for (String key : keys) {
					redisTemplate.delete(key);
				}
			}

		} finally {
			RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
		}
	}
}
