package com.heeexy.example.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.dao.PVPDao;
import com.heeexy.example.feign.RemoteServer;
import com.heeexy.example.service.PVPService;
import com.heeexy.example.util.CommonUtil;
import com.heeexy.example.util.StringTools;
import com.heeexy.example.util.model.Pvp;
import com.heeexy.example.util.model.Reasearch;
import com.heeexy.example.util.model.Tournament;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
@Service
public class PVPServiceImpl implements PVPService {

	@Autowired
	private PVPDao tDao;

	@Autowired
	RemoteServer remoteServer;

	@Autowired
	RedisTemplate redisTemplate;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	/**
	 * 上线
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject updateStatus(JSONObject jsonObject) {
		String status = jsonObject.getString("status");
		String id = jsonObject.getString("id");
		int pid = Integer.parseInt(id);
		if ("2".equals(status)) {
			Pvp p = tDao.selectById(pid);
			Date startTime = p.getStartTime();
			Date endTime = p.getEndTime();
			String one = dateFormat.format(startTime);
			String two = dateFormat.format(endTime);
			Map map = new HashMap();
			map.put("id", jsonObject.getString("id"));
			map.put("status", jsonObject.getString("status"));
			map.put("onLineTime", new Date());
			remoteServer.onlinePvp(pid, one, two);
			tDao.updateOnLine(map);
		} else if ("3".equals(status)) {
			Map map = new HashMap();
			map.put("id", jsonObject.getString("id"));
			map.put("status", jsonObject.getString("status"));
			map.put("offLineTime", new Date());
			remoteServer.offlinePvp(pid);
			tDao.updateOffLine(map);
		}
		return CommonUtil.successJson();
	}
	/**
	 * 上线
	 */
	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public JSONObject updateOffLine(JSONObject jsonObject) {
	// tDao.updateOffLine(jsonObject);
	// return CommonUtil.successJson();
	// }

	/**
	 * 
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject addPVP(JSONObject jsonObject) {
		int maxForder = tDao.selectMaxForder();
		Integer integer = jsonObject.getInteger("pvpType");
		if(integer==3 || integer ==4) {
			
			Tournament tr = new Tournament();
			tr.setEventName(jsonObject.getString("eventName"));
			tr.setPvpType(jsonObject.getInteger("pvpType"));
			tr.setChannel(jsonObject.getString("channel"));
			tr.setGameId(jsonObject.getString("gameId"));
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Long time = new Long(jsonObject.getString("startTime"));
			Long end = new Long(jsonObject.getString("endTime"));

			String d = sf.format(time);
			String s = sf.format(end);

			try {
				Date date = sf.parse(d);
				Date endDate = sf.parse(s);
				tr.setStartTime(date);
				tr.setEndTime(endDate);

			} catch (ParseException e) {
				e.printStackTrace();
			}
			tr.setCreateTime(new Date());
			tr.setRewardType(jsonObject.getString("rewardType"));
			tr.setEntryType(jsonObject.getString("entryType"));
			tr.setRoundTime(jsonObject.getIntValue("roundTime"));
			tr.setBenefitTimes("0");
			tr.setRoundCount("1");
			tr.setRake(BigDecimal.ZERO);
			tr.setBotSupport(0);
			tr.setBotNumbers(0);
			tr.setCoinPoolSource(2);
			String id = jsonObject.getString("id");
			tr.setId(Integer.parseInt(id));
			if ("0".equals(id)) {
				tr.setForder(maxForder+1);
				 tDao.addPVP(tr);
			} else {
				tDao.updatePVP(tr);
			}
			return CommonUtil.successJson();
		}

		// String 判断 jsonObject 必要字段
		String coinPoolSource = jsonObject.getString("coinPoolSource");
		Tournament t = new Tournament();
		t.setPvpType(jsonObject.getInteger("pvpType"));
		t.setBotSupport(jsonObject.getInteger("botSupport"));
		t.setBotNumbers(jsonObject.getInteger("botNumbers"));
		t.setEventName(jsonObject.getString("eventName"));
		t.setGameId(jsonObject.getString("gameId"));
		t.setRewardType(jsonObject.getString("rewardType"));
		t.setPlatformBenefitType(Byte.valueOf(jsonObject.getString("rewardType")));
		t.setEntryProductId(jsonObject.getString("entryProductId"));
		t.setCoinPoolSource(Integer.parseInt(coinPoolSource));
		try {
			t.setEntryFee(new BigDecimal(jsonObject.getString("entryFee")));
		} catch (Exception e) {
		}
		t.setChannel(jsonObject.getString("channel"));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Long time = new Long(jsonObject.getString("startTime"));
		Long end = new Long(jsonObject.getString("endTime"));

		String d = format.format(time);
		String s = format.format(end);

		try {
			Date date = format.parse(d);
			Date endDate = format.parse(s);
			t.setStartTime(date);
			t.setEndTime(endDate);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		t.setCreateTime(new Date());
		String roundCount = jsonObject.getString("rule");
		if ("".equals(roundCount)) {
			t.setRoundCount(null);
		} else {
			t.setRoundCount(roundCount);
		}
		t.setEntryType(jsonObject.getString("entryType"));
		t.setRoundTime(jsonObject.getIntValue("roundTime"));
		t.setBenefitTimes(jsonObject.getString("benefitTimes"));
		t.setPvpType(jsonObject.getInteger("pvpType"));
		t.setBotSupport(jsonObject.getInteger("botSupport"));
		int botNumbers = jsonObject.getInteger("botNumbers");
		t.setBotNumbers(botNumbers);
		if (!StringTools.isNullOrEmpty(coinPoolSource)) {
			if (coinPoolSource.equals("1")) {// from platform 
				jsonObject.put("rake", "0");
				// jsonObject.put("entryFee", "0");
				// t.setEntryFee(new BigDecimal(0));
				t.setRake(new BigDecimal("0"));
				String benefit = jsonObject.getString("coinPool");
				if (StringUtils.isNotEmpty(benefit)) {
					try {
						t.setBenefit(new BigDecimal(benefit));
					} catch (Exception e) {

					}
				}
				// jsonObject.put("benefit", benefit);

			} else if (coinPoolSource.equals("2")) {// from player 
				double rake = jsonObject.getDouble("rake");
				// jsonObject.put("rake", );
				String benefit = jsonObject.getString("benefit");

				if (StringUtils.isNotEmpty(benefit)) {
					try {
						t.setBenefit(new BigDecimal(benefit));
					} catch (Exception e) {

					}
				}

				t.setRake(new BigDecimal(rake / 100));

			}
		}

		BigDecimal entryFee = t.getEntryFee();
		BigDecimal benefit = t.getBenefit();
		BigDecimal rake = t.getRake();
		if (rake == null) {
			rake = BigDecimal.ZERO;
		}
		BigDecimal subtract = BigDecimal.ONE.subtract(rake);
		BigDecimal add = entryFee.multiply(new BigDecimal(2)).multiply(subtract).add(benefit);
		t.setReward(add);

		System.out.println("requestJson-----list2" + jsonObject);
		// jsonObject.put("benefit", new BigDecimal(2));
		String id = jsonObject.getString("id");
		t.setId(Integer.parseInt(id));
		if (t.getEntryProductId() == null) {
			System.out.println("====================================productId is null");
		}
		Integer PvpId = null;
		if ("0".equals(id)) {
			t.setForder(maxForder+1);
			 tDao.addPVP(t);
			 PvpId=t.getId();
		} else {
			tDao.updatePVP(t);
			PvpId=t.getId();
		}
		StringBuilder rKey = new StringBuilder();
		rKey.append("pvp_botNumbers_");
		rKey.append(PvpId);
		try {
			RedisConnectionUtils.bindConnection(redisTemplate.getConnectionFactory());
			redisTemplate.opsForValue().increment(rKey.toString(), botNumbers);
		} finally {
			RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
		}
		return CommonUtil.successJson();
	}

	/**
	 * 游戏列表
	 */
	@Override
	public JSONObject listGame(JSONObject jsonObject) {
		// CommonUtil.fillPageParam(jsonObject);
		// int count = tDao.countTournament(jsonObject);
		// List<JSONObject> list = tDao.listGame(jsonObject);
		// return CommonUtil.successPage(jsonObject, list, count);
		List<JSONObject> list = tDao.listPVP(null);
		return CommonUtil.successPage(list);
	}

	/**
	 * Tournament列表
	 */
	@Override
	public JSONObject listPVP(JSONObject jsonObject) {
		CommonUtil.fillPageParam(jsonObject);
		// int count = cmsDao.countGame(jsonObject);
		String status = jsonObject.getString("status");
		String entryType = jsonObject.getString("entryType");
		Date startTime = StringTools.changeDate(jsonObject.getString("startTime"));
		Date endTime = StringTools.changeDate(jsonObject.getString("endTime"));
		String name = jsonObject.getString("name");
		String gameId = jsonObject.getString("gameId");
		String channel = jsonObject.getString("channel");

		String offSet = jsonObject.getString("offSet");
		String pageRow = jsonObject.getString("pageRow");
		System.out.println("requestJson-----list" + jsonObject);

		Reasearch r = new Reasearch();

		if (!StringTools.isNullOrEmpty(status)) {
			r.setStatus(status);
		}
		if (!StringTools.isNullOrEmptyOrNegative(entryType)) {
			r.setEntryType(entryType);
		}
		if (startTime != null) {
			r.setStartTime(startTime);
		}
		if (endTime != null) {
			r.setEndTime(endTime);
		}
		if (!StringTools.isNullOrEmpty(name)) {
			r.setName(name);
		}
		if (!StringTools.isNullOrEmptyOrNegative(gameId)) {
			r.setGameId(gameId);
		}
		if (!StringTools.isNullOrEmptyOrNegative(channel)) {
			r.setChannel(channel);
		}
		if (!StringTools.isNullOrEmpty(offSet)) {
			r.setOffSet(Integer.parseInt(offSet));
		}
		if (!StringTools.isNullOrEmpty(pageRow)) {
			r.setPageRow(Integer.parseInt(pageRow));
		}

		// String participants = jsonObject.getString("participants");
		// String coinPool = jsonObject.getString("coinPool");
		// String orderBy ;
		//
		// String orderByString = " order by ";
		// if(!StringTools.isNullOrEmpty(participants)) {
		//// if(participants.contains("asc")) {
		//// r.setAsc("asc");
		//// }
		//// if(participants.contains("desc")) {
		//// r.setDesc("asc");
		//// }
		// orderByString += " tr.participants " + participants;
		// if(!StringTools.isNullOrEmpty(coinPool)) {
		// orderByString += " , tr.coinPool "+ coinPool;
		// }
		// }else {
		// if(!StringTools.isNullOrEmpty(coinPool)) {
		// orderByString += " tr.coinPool "+ coinPool;
		// }else {// 没有排序需求,清空
		// orderByString = "";
		// }
		// }
		// System.out.println("requestJson-----orderByString"+orderByString);
		//
		// if(!StringTools.isNullOrEmpty(orderByString)) {
		// //jsonObject.put("orderBy", orderByString);
		// r.setOrderBy(orderByString+" ");
		// }
		// System.out.println("requestJson-----setOrderBy"+r.getOrderBy()+"==");
		String orderByString = "  ";
		String coinPool = jsonObject.getString("coinPool");
		System.out.println("requestJson-----list------" + coinPool);

		if (!StringTools.isNullOrEmptyOrNegative(coinPool)) {
			orderByString += " order by  tr.coinPool " + coinPool;
		} else {
			orderByString = " order by startTime ";
		}
		if (!StringTools.isNullOrEmpty(orderByString)) {
			// jsonObject.put("orderBy", orderByString);
			r.setOrderBy(orderByString + " ");
		}

		int count = tDao.countPVP(r);
		// System.out.println("requestJson-----list"+r.getOffSet());
		// System.out.println("requestJson-----list"+r.getPageRow());
		// r.setStatus("1");
		List<JSONObject> list = tDao.listPVP(r);
		System.out.println("requestJson-----list" + list.size());

		return CommonUtil.successPage(jsonObject, list, count);
	}

	@Override
	public Pvp selectByTid(Integer tid) {
		return tDao.selectById(tid);
	}

	/**
	 * 更新游戏状态
	 */
	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public JSONObject updateGameStatus(JSONObject jsonObject) {
	// tDao.updateGameStatus(jsonObject);
	// return CommonUtil.successJson();
	// }
}
