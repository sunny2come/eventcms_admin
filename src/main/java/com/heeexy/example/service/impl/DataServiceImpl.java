package com.heeexy.example.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.dao.ArticleDao;
import com.heeexy.example.dao.CMSDao;
import com.heeexy.example.dao.DataDao;
import com.heeexy.example.dao.PVPDao;
import com.heeexy.example.dao.TournamentDao;
import com.heeexy.example.service.ArticleService;
import com.heeexy.example.service.CMSService;
import com.heeexy.example.service.DataService;
import com.heeexy.example.service.PVPService;
import com.heeexy.example.service.TournamentService;
import com.heeexy.example.util.CommonUtil;
import com.heeexy.example.util.StringTools;
import com.heeexy.example.util.ExcelUtil.ExcelHead;
import com.heeexy.example.util.ExcelUtil.ExcelUser;
import com.heeexy.example.util.ExcelUtil.ExcelUtils;
import com.heeexy.example.util.constants.Constants;
import com.heeexy.example.util.excel.CellMapping;
import com.heeexy.example.util.model.Reasearch;
import com.heeexy.example.util.model.Tournament;
import com.heeexy.example.util.model.TournamentRewardTemplate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
@Service
public class DataServiceImpl implements DataService {

	@Autowired
	private DataDao tDao;

	JSONObject alJson = new JSONObject();
	int count = 0;
	int event = 0;
	int participants = 0;
	int personTime = 0;

	int aountBeans = 0;
	int platformGrossBeans = 0;

	int aountCrash = 0;
	int platformGrossCrash = 0;

	int failedEvent = 0;

	/**
	 * 上线
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject listPlatForm(JSONObject jsonObject) {

		CommonUtil.fillPageParam(jsonObject);

		String gameType = jsonObject.getString("gameType");
		String startTime = jsonObject.getString("startTime");
		String endTime = jsonObject.getString("endTime");

		// 清空操作
		alJson = new JSONObject();
		count = 0;
		event = 0;
		participants = 0;
		personTime = 0;

		aountBeans = 0;
		platformGrossBeans = 0;

		aountCrash = 0;
		platformGrossCrash = 0;

		failedEvent = 0;

		List<JSONObject> list = new ArrayList<JSONObject>();
		if ("1".equals(gameType)) {// Tournament
			// data event
			getTournamentJson(jsonObject, list);
			count = tDao.countTournamentList(jsonObject);
			// list.add(e)
		} else if ("2".equals(gameType)) {
			getPVPJson(jsonObject, list);
			count = tDao.countPVPList(jsonObject);
		} else {// 所有
			count += tDao.countTournamentList(jsonObject);
			count += tDao.countPVPList(jsonObject);

			getTournamentJson(jsonObject, list);
			getPVPJson(jsonObject, list);
		}
		return CommonUtil.successPageByALL(jsonObject, alJson, list, count);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public JSONObject listGame(JSONObject jsonObject) {

		String gameType = jsonObject.getString("gameType");

		JSONObject item = new JSONObject();
		if ("1".equals(gameType)) {// Tournament
			// data event
			// result.startTime as data,
			// sum(participants) as participants,
			// sum(personTime) as personTime
			// sum(r.aount) as aount,
			// sum(platformGross) as platformGross
			// bean
			// Crash

			item.put("event", tDao.countTournament(jsonObject));
			
			JSONObject item2 = tDao.countPersonTournament(jsonObject);
			if (item2 != null) {
				item.put("participants", item2.get("participants"));
				item.put("personTime", item2.get("personTime"));
			} else {
				item.put("participants", 0);
				item.put("personTime", 0);
			}
			jsonObject.put("entryType", 1);
			JSONObject itemBean = tDao.runningAccountTournament(jsonObject);
			
			if (item2 != null) {
				item.put("aountBeans", itemBean.get("aount"));
				item.put("platformGrossBeans", itemBean.get("platformGross"));
			} else {
				item.put("aountBeans", 0);
				item.put("platformGrossBeans", 0);
			}
			jsonObject.put("entryType", 2);
			JSONObject itemCrash = tDao.runningAccountTournament(jsonObject);
			if (item2 != null) {
				item.put("aountCrash", itemCrash.get("aount"));
				item.put("platformGrossCrash", itemCrash.get("platformGross"));
			} else {
				item.put("aountCrash", 0);
				item.put("platformGrossCrash", 0);
			} 
			/**
			 * 
			 * failedEvent
			 * 
			 */
			int failedItem = tDao.countFailedTournament(jsonObject);
			item.put("failedEvent", failedItem);

			// list.add(e)
		} else if ("2".equals(gameType)) {
			item.put("event", tDao.countPVP(jsonObject));
			
			JSONObject item2 = tDao.countPersonPVP(jsonObject);
			if (item2 != null) {
				item.put("participants", item2.get("participants"));
				item.put("personTime", item2.get("personTime"));
			} else {
				item.put("participants", 0);
				item.put("personTime", 0);
			}
			jsonObject.put("entryType", 1);
			JSONObject itemBean = tDao.runningAccountPVP(jsonObject);
			
			if (item2 != null) {
				item.put("aountBeans", itemBean.get("aount"));
				item.put("platformGrossBeans", itemBean.get("platformGross"));
			} else {
				item.put("aountBeans", 0);
				item.put("platformGrossBeans", 0);
			}
			jsonObject.put("entryType", 2);
			JSONObject itemCrash = tDao.runningAccountPVP(jsonObject);
			if (item2 != null) {
				item.put("aountCrash", itemCrash.get("aount"));
				item.put("platformGrossCrash", itemCrash.get("platformGross"));
			} else {
				item.put("aountCrash", 0);
				item.put("platformGrossCrash", 0);
			}
			// failedEvent
			int failedItem = tDao.countFailedPVP(jsonObject);
			item.put("failedEvent", failedItem);
		}
		return CommonUtil.successJson(item);
	}

	// TODO 有待优化
	private JSONObject getListNum(List<JSONObject> list, String data) {
		for (JSONObject item : list) {
			if (data.equals(item.get("data"))) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 累加到 list里
	 * 
	 * @param jsonObject
	 * @param list
	 */
	private void getPVPJsonADD(JSONObject jsonObject, List<JSONObject> list) {
		List<JSONObject> tList = tDao.countPVPByDate(jsonObject);

		// result.startTime as data,
		// sum(participants) as participants,
		// sum(personTime) as personTime
		List<JSONObject> tpList = tDao.countPersonPVPByDate(jsonObject);
		// sum(r.aount) as aount,
		// sum(platformGross) as platformGross
		// bean
		jsonObject.put("entryType", 1);
		List<JSONObject> trListBean = tDao.runningAccountPVPByDate(jsonObject);
		// Crash
		jsonObject.put("entryType", 2);
		List<JSONObject> trListCrash = tDao.runningAccountPVPByDate(jsonObject);

		// failedEvent
		List<JSONObject> tfList = tDao.countFailedPVPByDate(jsonObject);
		for (JSONObject o : tList) {
			JSONObject item = new JSONObject();
			String data = (String) o.get("data");
			item.put("data", data);
			item.put("event", o.get("event"));

			event += (int) o.get("event");

			JSONObject item2 = getListNum(tpList, data);
			if (item2 != null) {
				item.put("participants", item2.get("participants"));
				item.put("personTime", item2.get("personTime"));
				participants += (int) item2.get("participants");
				personTime += (int) item2.get("personTime");

			} else {
				item.put("participants", 0);
				item.put("personTime", 0);
			}

			JSONObject itemBean = getListNum(trListBean, data);
			if (itemBean != null) {
				item.put("aountBeans", itemBean.get("aount"));
				item.put("platformGrossBeans", itemBean.get("platformGross"));
				aountBeans += (int) itemBean.get("aount");
				platformGrossBeans += (int) itemBean.get("platformGross");

			} else {
				item.put("aountBeans", 0);
				item.put("platformGrossBeans", 0);
			}
			JSONObject itemCrash = getListNum(trListCrash, data);
			if (itemCrash != null) {
				item.put("aountCrash", itemCrash.get("aount"));
				item.put("platformGrossCrash", itemCrash.get("platformGross"));
				aountCrash += (int) itemCrash.get("aount");
				platformGrossCrash += (int) itemCrash.get("platformGross");
			} else {
				item.put("aountCrash", 0);
				item.put("platformGrossCrash", 0);
			}
			JSONObject failedItem = getListNum(tfList, data);
			if (failedItem != null) {
				item.put("failedEvent", failedItem.get("failedEvent"));
				failedEvent += (int) failedItem.get("failedEvent");
			} else {
				item.put("failedEvent", 0);
			}

			list.add(item);
		}
	}

	private void getPVPJson(JSONObject jsonObject, List<JSONObject> list) {
		List<JSONObject> tList = tDao.countPVPByDate(jsonObject);
		// result.startTime as data,
		// sum(participants) as participants,
		// sum(personTime) as personTime
		List<JSONObject> tpList = tDao.countPersonPVPByDate(jsonObject);
		// sum(r.aount) as aount,
		// sum(platformGross) as platformGross
		// bean
		jsonObject.put("entryType", 1);
		List<JSONObject> trListBean = tDao.runningAccountPVPByDate(jsonObject);
		// Crash
		jsonObject.put("entryType", 2);
		List<JSONObject> trListCrash = tDao.runningAccountPVPByDate(jsonObject);

		// failedEvent
		List<JSONObject> tfList = tDao.countFailedPVPByDate(jsonObject);
		for (JSONObject o : tList) {
			JSONObject item = new JSONObject();
			String data = (String) o.get("data");
			item.put("data", data);
			item.put("event", o.get("event"));

			event += (int) o.get("event");

			JSONObject item2 = getListNum(tpList, data);
			if (item2 != null) {
				item.put("participants", item2.get("participants"));
				item.put("personTime", item2.get("personTime"));
				participants += (int) item2.get("participants");
				personTime += (int) item2.get("personTime");

			} else {
				item.put("participants", 0);
				item.put("personTime", 0);
			}

			JSONObject itemBean = getListNum(trListBean, data);
			if (itemBean != null) {
				item.put("aountBeans", itemBean.get("aount"));
				item.put("platformGrossBeans", itemBean.get("platformGross"));
				aountBeans += (int) itemBean.get("aount");
				platformGrossBeans += (int) itemBean.get("platformGross");

			} else {
				item.put("aountBeans", 0);
				item.put("platformGrossBeans", 0);
			}
			JSONObject itemCrash = getListNum(trListCrash, data);
			if (itemCrash != null) {
				item.put("aountCrash", itemCrash.get("aount"));
				item.put("platformGrossCrash", itemCrash.get("platformGross"));
				aountCrash += (int) itemCrash.get("aount");
				platformGrossCrash += (int) itemCrash.get("platformGross");
			} else {
				item.put("aountCrash", 0);
				item.put("platformGrossCrash", 0);
			}
			JSONObject failedItem = getListNum(tfList, data);
			if (failedItem != null) {
				item.put("failedEvent", failedItem.get("failedEvent"));
				failedEvent += (int) failedItem.get("failedEvent");
			} else {
				item.put("failedEvent", 0);
			}

			list.add(item);
		}
	}

	private void getTournamentJson(JSONObject jsonObject, List<JSONObject> list) {
		List<JSONObject> tList = tDao.countTournamentByDate(jsonObject);
		// result.startTime as data,
		// sum(participants) as participants,
		// sum(personTime) as personTime
		List<JSONObject> tpList = tDao.countPersonTournamentByDate(jsonObject);
		// sum(r.aount) as aount,
		// sum(platformGross) as platformGross
		// bean
		jsonObject.put("entryType", 1);
		List<JSONObject> trListBean = tDao.runningAccountTournamentByDate(jsonObject);
		// Crash
		jsonObject.put("entryType", 2);
		List<JSONObject> trListCrash = tDao.runningAccountTournamentByDate(jsonObject);

		// failedEvent
		List<JSONObject> tfList = tDao.countFailedTournamentByDate(jsonObject);
		for (JSONObject o : tList) {
			JSONObject item = new JSONObject();
			String data = (String) o.get("data");
			item.put("data", data);
			item.put("event", o.get("event"));

			event += (int) o.get("event");

			JSONObject item2 = getListNum(tpList, data);
			if (item2 != null) {
				item.put("participants", item2.get("participants"));
				item.put("personTime", item2.get("personTime"));
				participants += (int) item2.get("participants");
				personTime += (int) item2.get("personTime");

			} else {
				item.put("participants", 0);
				item.put("personTime", 0);
			}

			JSONObject itemBean = getListNum(trListBean, data);
			if (itemBean != null) {
				item.put("aountBeans", itemBean.get("aount"));
				item.put("platformGrossBeans", itemBean.get("platformGross"));
				aountBeans += (int) itemBean.get("aount");
				platformGrossBeans += (int) itemBean.get("platformGross");

			} else {
				item.put("aountBeans", 0);
				item.put("platformGrossBeans", 0);
			}
			JSONObject itemCrash = getListNum(trListCrash, data);
			if (itemCrash != null) {
				item.put("aountCrash", itemCrash.get("aount"));
				item.put("platformGrossCrash", itemCrash.get("platformGross"));
				aountCrash += (int) itemCrash.get("aount");
				platformGrossCrash += (int) itemCrash.get("platformGross");
			} else {
				item.put("aountCrash", 0);
				item.put("platformGrossCrash", 0);
			}
			JSONObject failedItem = getListNum(tfList, data);
			if (failedItem != null) {
				item.put("failedEvent", failedItem.get("failedEvent"));
				failedEvent += (int) failedItem.get("failedEvent");
			} else {
				item.put("failedEvent", 0);
			}

			list.add(item);
		}
	}
}
