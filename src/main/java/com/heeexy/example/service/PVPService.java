package com.heeexy.example.service;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.util.model.Pvp;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
public interface PVPService {

	/**
	 * 游戏列表
	 */
	JSONObject listGame(JSONObject jsonObject);

	/**
	 * Tournament列表
	 */
	JSONObject listPVP(JSONObject jsonObject);

	/**
	 * 新增Tournament
	 */
	JSONObject addPVP(JSONObject jsonObject);

	/**
	 * 上线
	 */
	JSONObject updateStatus(JSONObject jsonObject);


	Pvp selectByTid(Integer tid);

}
