package com.heeexy.example.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

/**
 * @author: jonny
 * @date: 2019 02.20
 */
public interface DataService {

	/**
	 * 平台维度
	 */
	JSONObject listPlatForm(JSONObject jsonObject);
	/**
	 * 游戏维度
	 */
	JSONObject listGame(JSONObject jsonObject);

	
}
