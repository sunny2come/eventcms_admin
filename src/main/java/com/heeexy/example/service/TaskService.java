package com.heeexy.example.service;

import java.util.List;

import com.heeexy.example.controller.Task;

public interface TaskService {

	List<Task> getTaskList();

	void cacheTask();

	void loadTask();

	void createTask(String type, int sourceId, String date);

	void cancelTask(String type, Integer sourceId);

}
