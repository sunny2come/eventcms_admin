package com.heeexy.example.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {

	@Value("${domain}")
	String domain;
	
	public String iconUpload(MultipartFile file) {
		if (Objects.isNull(file) || file.isEmpty()) {
			System.out.println("文件为空");
			return "文件为空，请重新上传";
		}
		try {
			byte[] bytes = file.getBytes();
			String originalFilename = file.getOriginalFilename();
			String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
			String fileName = new Date().getTime() + suffix;
			Path path = Paths.get("iconImg/" + fileName);
			// 如果没有files文件夹，则创建
			if (!Files.isWritable(path)) {
				Files.createDirectories(Paths.get("iconImg/"));
			}
			// 文件写入指定路径
			Files.write(path, bytes);
			System.out.println("文件写入成功...");
			String iconUrl = domain + "/cms/icon/" + fileName;
			return iconUrl;
		} catch (IOException e) {
			e.printStackTrace();
			// 后端异常...
			return "-2";
		}
	}

}
