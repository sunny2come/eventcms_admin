package com.heeexy.example.dao;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * @author: jonny
 * @description: DataDao层
 * @date: 2019/02/20 16:06
 */
public interface DataDao {
	
	/**
	 * 获得
	 * @param jsonObject
	 * @return
	 */
	int countTournamentList(JSONObject jsonObject);

	int countPVPList(JSONObject jsonObject);

	
	//活动总数：成功激活上架的总数(finish+正在上线的)
	int countTournament(JSONObject jsonObject);
	
	int countPVP(JSONObject jsonObject);

	List<JSONObject> countTournamentByDate(JSONObject jsonObject);

	List<JSONObject> countPVPByDate(JSONObject jsonObject);

	//参与人数：参加活动的总人数  (tournament人数和2*PVP)
	JSONObject countPersonTournament(JSONObject jsonObject);

	JSONObject countPersonPVP(JSONObject jsonObject);

	List<JSONObject> countPersonTournamentByDate(JSONObject jsonObject);

	List<JSONObject> countPersonPVPByDate(JSONObject jsonObject);
	
	/**
	 * 流水数据
	 * 
	 *   beans：入场费总数（ finished活动的入场费）
	 *   crash
	 *   
	 *   分成后收入：
	 * 				Beans ：入场费总数（ finished活动的入场费）* rake
	 *         Cash：同上 
	 *		entryType 1 为 bean 2 crash
	 *
	 */
	JSONObject runningAccountTournament(JSONObject jsonObject);

	List<JSONObject> runningAccountTournamentByDate(JSONObject jsonObject);
	
	JSONObject runningAccountPVP(JSONObject jsonObject);

	List<JSONObject> runningAccountPVPByDate(JSONObject jsonObject);

	//
	/**
	 * 未开通活动数（未满足最小人数要求
	 */
	int countFailedTournament(JSONObject jsonObject);
	
	int countFailedPVP(JSONObject jsonObject);

	List<JSONObject> countFailedTournamentByDate(JSONObject jsonObject);

	List<JSONObject> countFailedPVPByDate(JSONObject jsonObject);
	
}
