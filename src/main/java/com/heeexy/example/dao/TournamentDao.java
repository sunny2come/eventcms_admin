package com.heeexy.example.dao;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.util.model.Reasearch;
import com.heeexy.example.util.model.Tournament;
import com.heeexy.example.util.model.TournamentRewardTemplate;

import java.util.List;
import java.util.Map;

/**
 * @author: jonny
 * @description: TournamentDao层
 * @date: 2019/02/22 16:06
 */
public interface TournamentDao {

	/**
	 * 游戏列表
	 */
	List<JSONObject> listGame(JSONObject jsonObject);

	/**
	 * 游戏对应Tournament列表
	 */
	List<JSONObject> listTournament(Reasearch reasearch);

	/**
	 * 统计游戏总数
	 */
	int countTournament(Reasearch reasearch);

	int addTournament(Tournament t);

	int updateOnLine(Map map);

	int updateOffLine(Map map);

	int updateTournament(Tournament t);

	int addTournamentRewardTemplate(TournamentRewardTemplate t);

	String getFileNameById(Map map);

	int deleteTournamentRewardTemplateById(Integer i);

	Tournament selectById(int tid);

	int cloneTournament(Integer tid);

}
