package com.heeexy.example.dao;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.util.model.Pvp;
import com.heeexy.example.util.model.Reasearch;
import com.heeexy.example.util.model.Tournament;

import java.util.List;
import java.util.Map;

/**
 * @author: jonny
 * @description: TournamentDao层
 * @date: 2019/02/22 16:06
 */
public interface PVPDao {

	/**
	 * 游戏列表
	 */
	List<JSONObject> listGame(JSONObject jsonObject);
	/**
	 * 游戏对应Tournament列表
	 */
	List<JSONObject> listPVP(Reasearch reasearch);
	
	/**
	 * 统计游戏总数
	 */
	int countPVP(Reasearch reasearch);
	
	int addPVP(Tournament t);
	
	int updateOnLine(Map map);
	
	int updateOffLine(Map map);
	
	int updatePVP(Tournament t);
	Pvp selectById(int pid);
	int selectMaxForder();
	
}
