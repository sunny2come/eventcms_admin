package com.heeexy.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.util.model.Bot;
import com.heeexy.example.util.model.BotScoreRange;
import com.heeexy.example.util.model.Game;
import com.heeexy.example.util.model.vo.BotScoreRangeVO;

/**
 * @author: jonny
 * @description: CMSDao层
 * @date: 2019/02/20 16:06
 */
public interface CMSDao {

	/**
	 * 更新游戏状态
	 */
	int updateGameStatus(JSONObject jsonObject);

	/**
	 * 游戏列表
	 */
	List<JSONObject> listGame(JSONObject jsonObject);
	/**
	 * 游戏对应Tournament列表
	 */
	List<JSONObject> listTournament(JSONObject jsonObject);
	/**
	 * 游戏对应PVP列表
	 */
	List<JSONObject> listPVP(JSONObject jsonObject);
	
	/**
	 * 统计游戏总数
	 */
	int countGame(JSONObject jsonObject);
	List<JSONObject> selectProductIdByType(Integer type);
	List<Game> listAllGame();
	List<BotScoreRangeVO> listBotGameRange(Integer gameId);
	void batchInsertScore(List<BotScoreRange> tempScoreList);
	void deleteScores(Integer gameId);
	void batchInsertBot(List<Bot> list);
	List<Bot> allBotList();
	void truncateData(String tableName);
	int insertGame(Game game);
	int updateGame(Game game);
	
	List<JSONObject> listEvent(Integer gameId);
	void batchUpdateTnum(List<JSONObject> tournamentList);
	void batchUpdatePnum(List<JSONObject> pvpList);
	void batchUpdateGameForder(JSONArray jsonArray);

	List<Integer> influenceEvent(Integer gameId);

	void gameOfflineById(Integer gameId);

	int updateShamPlayTime(@Param("gameId")Integer gameId, @Param("shamPlayTime")Integer shamPlayTime);
}
