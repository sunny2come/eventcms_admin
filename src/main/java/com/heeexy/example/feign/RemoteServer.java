package com.heeexy.example.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.heeexy.example.controller.Task;

@Component
@FeignClient(name = "gamepind-event-job")
public interface RemoteServer {

	@RequestMapping(value = "/api/jobCenter/onlineTournament", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	void onlineTournament(@RequestParam("tournamentId") Integer tournamentId,
			@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime);

	@RequestMapping(value = "/api/jobCenter/offlineTournament", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	void offlineTournament(@RequestParam("tournamentId") Integer tournamentId);

	@RequestMapping(value = "/api/jobCenter/onlinePvp", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	void onlinePvp(@RequestParam("pvpId") Integer pvpId, @RequestParam("startTime") String startTime,
			@RequestParam("endTime") String endTime);

	@RequestMapping(value = "/api/jobCenter/offlinePvp", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	void offlinePvp(@RequestParam("pvpId") Integer pvpId);

	@RequestMapping(value = "/api/jobCenter/cacheTask", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public void cacheTask();

	@RequestMapping(value = "/api/jobCenter/loadTask", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public void loadTask();

	@RequestMapping(value = "/api/jobCenter/createTask", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public void createTask(@RequestParam("type")Integer type,@RequestParam("sourceId") int sourceId,@RequestParam("Date") String Date);

	@RequestMapping(value = "/api/jobCenter/cancelTask", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public void cancelTask(@RequestParam("type")Integer type,@RequestParam("sourceId") int sourceId);

	@RequestMapping(value = "/api/jobCenter/getTaskList", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public List<Task> getTaskList();
}
