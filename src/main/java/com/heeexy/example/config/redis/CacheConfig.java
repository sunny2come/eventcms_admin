package com.heeexy.example.config.redis;

import java.nio.charset.Charset;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class CacheConfig{
	@Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate _redisTemplate = new RedisTemplate();
        _redisTemplate.setConnectionFactory(connectionFactory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        _redisTemplate.setKeySerializer(stringRedisSerializer);
        _redisTemplate.setHashKeySerializer(new StringRedisSerializer(Charset.forName("UTF-8")));
        return _redisTemplate;
    }

}
