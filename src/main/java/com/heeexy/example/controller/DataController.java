package com.heeexy.example.controller;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.service.CMSService;
import com.heeexy.example.service.DataService;
import com.heeexy.example.util.CommonUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: jonny
 * @description: DATA相关Controller
 * @date: 2019/03/04 16:04
 */
@RestController
@RequestMapping("/data")
public class DataController {

	@Autowired
	private DataService dataService;

	/**
	 * 平台维度
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listPlatform")
	public JSONObject listPlatform(HttpServletRequest request) {
		return dataService.listPlatForm(CommonUtil.request2Json(request));
	}
	/**
	 * 游戏维度
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listGame")
	public JSONObject listGame(HttpServletRequest request) {
		return dataService.listPlatForm(CommonUtil.request2Json(request));
	}
	/**
	 * 查询Tournament列表
	 */
//	@RequiresPermissions("role:list")
//	@GetMapping("/listTournament")
//	public JSONObject listTournament(HttpServletRequest request) {
//		return dataService.listTournament(CommonUtil.request2Json(request));
//	}
//	
//	SELECT 
//	count(0)
//	FROM 
//	tournament
//	where status in (0,2);

}
