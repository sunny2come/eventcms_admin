package com.heeexy.example.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.service.PVPService;
import com.heeexy.example.util.CommonUtil;

/**
 * @author: jonny
 * @description: Tournament相关Controller
 * @date: 2019/02/24 16:04
 */
@RestController
@RequestMapping("/pvp")
public class PVPController {

	@Autowired
	private PVPService tourService;

	/**
	 * 查询列表
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listGame")
	public JSONObject listGame(HttpServletRequest request) {
		return tourService.listGame(CommonUtil.request2Json(request));
	}
	/**
	 * 查询Tournament列表
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listPVP")
	public JSONObject listPVP(HttpServletRequest request) {
		JSONObject listPVP = tourService.listPVP(CommonUtil.request2Json(request));
		System.out.println(listPVP);
		return listPVP;
	}

	/**
	 * 增加Tournament
	 */
	@RequiresPermissions("role:add")
	@PostMapping("/addPVP")
	public JSONObject addPVP(@RequestBody JSONObject requestJson) {
		Integer integer = requestJson.getInteger("pvpType");
		if(integer!=3 && integer !=4) {
			CommonUtil.hasAllRequired(requestJson, "coinPoolSource");
		}
		return tourService.addPVP(requestJson);
	}
	/**
	 * 增加Tournament
	 */
	@RequiresPermissions("role:add")
	@PostMapping("/updateStatus")
	public JSONObject updateStatus(@RequestBody JSONObject requestJson) {
		CommonUtil.hasAllRequired(requestJson, "id");
		return tourService.updateStatus(requestJson);
	}
	
//	@RequiresPermissions("role:list")
//	@GetMapping("/listEvents")
//	public JSONObject listEvents(HttpServletRequest request) {
//		JSONObject requestJson = CommonUtil.request2Json(request);
//		System.out.println("requestJson"+requestJson);
//		//CommonUtil.hasAllRequired(requestJson, "id,type");
//		//TODO 参数类型
//		String type = (String) requestJson.get("type");
//		System.out.println("requestJson"+type);
//		if("1".equals(type)) {// tournament
//			return cmsService.listTournament(requestJson);
//		}else if("2".equals(type)) {//pvp
//			JSONObject jsonN = cmsService.listPVP(requestJson);
//			System.out.println("jsonN"+jsonN);
//			return jsonN;
//		}
//		return cmsService.listTournament(requestJson);
//	}
}
