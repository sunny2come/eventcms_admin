package com.heeexy.example.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.heeexy.example.service.TournamentService;
import com.heeexy.example.util.CommonUtil;
import com.heeexy.example.util.StringTools;
import com.heeexy.example.util.ExcelUtil.ExcelHead;
import com.heeexy.example.util.ExcelUtil.ExcelUtils;
import com.heeexy.example.util.model.TournamentRewardTemplate;

/**
 * @author: jonny
 * @description: Tournament相关Controller
 * @date: 2019/02/24 16:04
 */
@RestController
@RequestMapping("/tournament")
//@CrossOrigin
public class TournamentController {

	@Autowired
	private TournamentService tourService;

	/**
	 * 查询列表
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listGame")
	public JSONObject listGame(HttpServletRequest request) {
		return tourService.listGame(CommonUtil.request2Json(request));
	}
	@RequiresPermissions("role:list")
	@RequestMapping(value = "/clone", method = RequestMethod.POST)
	public JSONObject clone(@RequestBody Map<String, String> map) {
		String tid = map.get("tid");
		return tourService.clone(Integer.valueOf(tid));
	}
	
	
	@RequiresPermissions("role:list")
	@GetMapping("/download")
    public String downLoad(HttpServletRequest request,HttpServletResponse response){
        String filename="20190301161036.xlsx";
        System.out.println("----------file download" + filename);

        JSONObject json =   CommonUtil.request2Json(request);
        String filePath = "excel" ;
        System.out.println("----------file download=====" + json);

        filename = json.getString("fileName");
        File file = new File(filePath + "/" + filename);
        System.out.println("----------file download2" + filename);

        if(file.exists()){ //判断文件父目录是否存在
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment;fileName=" + filename);
            System.out.println("----------file download3" + filename);

            byte[] buffer = new byte[1024];
            FileInputStream fis = null; //文件输入流
            BufferedInputStream bis = null;
            
            OutputStream os = null; //输出流
            try {
                os = response.getOutputStream();
                fis = new FileInputStream(file); 
                bis = new BufferedInputStream(fis);
                int i = bis.read(buffer);
                while(i != -1){
                    os.write(buffer);
                    i = bis.read(buffer);
                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("----------file download--------" + filename);
            try {
                bis.close();
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
	/**
	 * 查询Tournament列表
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listTournament")
	public JSONObject listTournament(HttpServletRequest request) {
//		1、根目录所对应的绝对路径：request.getRequestURI()；
//
//		2、文件的绝对路径：application.getRealPath(request.getRequestURI())
//
//		3、当前web应用的绝对路径：application.getRealPath("/")
		return tourService.listTournament(CommonUtil.request2Json(request));
	}

	/**
	 * 增加Tournament
	 */
	@RequiresPermissions("role:add")
	@PostMapping("/addTournament")
	public JSONObject addTournament(@RequestBody JSONObject requestJson) {
		CommonUtil.hasAllRequired(requestJson, "coinPoolSource");
		return tourService.addTournament(requestJson);
	}

	/**
	 * 增加Tournament
	 */
	@RequiresPermissions("role:add")
	@PostMapping("/updateStatus")
	public JSONObject updateStatus(@RequestBody JSONObject requestJson) {
		CommonUtil.hasAllRequired(requestJson, "id");
		return tourService.updateStatus(requestJson);
	}
    private String UPLOAD_FOLDER ="excel/";

//    @CrossOrigin(origins = "http://30.27.186.4:8080")
	@RequiresPermissions("role:add")
	@PostMapping("/uploadFile")
	public String uploadFile(MultipartFile file) {
//		System.out.println("传入的文件参数：{}"+JSON.toJSONString(file, true));
		if (Objects.isNull(file) || file.isEmpty()) {
			System.out.println("文件为空");
			return "文件为空，请重新上传";
		}
		String fileName = new SimpleDateFormat("yyyyMMddHHmmss'.xlsx'").format(new Date());
		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOAD_FOLDER + fileName);
			// 如果没有files文件夹，则创建
			if (!Files.isWritable(path)) {
				Files.createDirectories(Paths.get(UPLOAD_FOLDER));
			}
			// 文件写入指定路径
			Files.write(path, bytes);
			System.out.println("文件写入成功...");
//			if(checkExcel(fileName)) {
				System.out.println("fileName------"+fileName);

				return fileName;
//			} else {
//				//奖励总和不为100%
//				System.out.println("-------1");
//
//				return "-1";
//			}
//			return fileName;
		} catch (IOException e) {
			e.printStackTrace();
			//后端异常...
			return "-2";
		}
	}
	private boolean checkExcel(String  fileName) {
		File file = new File("excel/"+fileName);
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);

			List<ExcelHead> excelHeads = new ArrayList<ExcelHead>();
			ExcelHead excelHead = new ExcelHead("Winner Rank", "rank");
			ExcelHead excelHead1 = new ExcelHead("No. of winners", "numofwin");
			ExcelHead excelHead2 = new ExcelHead("Prize percentage(%)", "prizePercentage");
			ExcelHead excelHead3 = new ExcelHead("Total prize percentage(%)", "totalPrizePercentage");
			// ExcelHead excelHead3 = new ExcelHead("Total prize percentage", "address",
			// true);
			excelHeads.add(excelHead);
			excelHeads.add(excelHead1);
			excelHeads.add(excelHead2);
			excelHeads.add(excelHead3);
			List<TournamentRewardTemplate> list = ExcelUtils.readExcelToEntity(TournamentRewardTemplate.class, in,
					file.getName(), excelHeads);
			int pAll = 0;
			int toAll = 0;
			for (TournamentRewardTemplate item : list) {
				if(!StringTools.isNullOrEmpty(item.getPrizePercentage())) {
					pAll += item.getNumofwin()*Double.valueOf(item.getPrizePercentage());
					toAll += Double.valueOf(item.getPrizePercentage());
				}
				//item.setTouramentId(new Integer(tournamentId));
			}
			System.out.println("pAll------"+pAll);
			System.out.println("toAll------"+toAll);

			if(100==pAll&&100==toAll) {
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	// @RequiresPermissions("role:list")
	// @GetMapping("/listEvents")
	// public JSONObject listEvents(HttpServletRequest request) {
	// JSONObject requestJson = CommonUtil.request2Json(request);
	// System.out.println("requestJson"+requestJson);
	// //CommonUtil.hasAllRequired(requestJson, "id,type");
	// //TODO 参数类型
	// String type = (String) requestJson.get("type");
	// System.out.println("requestJson"+type);
	// if("1".equals(type)) {// tournament
	// return cmsService.listTournament(requestJson);
	// }else if("2".equals(type)) {//pvp
	// JSONObject jsonN = cmsService.listPVP(requestJson);
	// System.out.println("jsonN"+jsonN);
	// return jsonN;
	// }
	// return cmsService.listTournament(requestJson);
	// }
}
