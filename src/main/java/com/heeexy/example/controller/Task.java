package com.heeexy.example.controller;

public class Task {

	private String message;

	public Task(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
