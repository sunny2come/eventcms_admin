package com.heeexy.example.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.heeexy.example.service.PVPService;
import com.heeexy.example.service.TaskService;
import com.heeexy.example.service.TournamentService;
import com.heeexy.example.util.model.Pvp;
import com.heeexy.example.util.model.Tournament;

@RestController
@RequestMapping("/task")
public class RemoteTaskController {

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private TournamentService tournamentService;
	
	@Autowired
	private PVPService pvpService;

	@RequiresPermissions("role:list")
	@RequestMapping(value = "/getTaskList", method = RequestMethod.GET)
	@ResponseBody
	public List<Task> getTaskList() {
		return taskService.getTaskList();
	}

	@RequiresPermissions("role:list")
	@RequestMapping(value = "/cacheTask", method = RequestMethod.GET)
	public void cacheTask() {
		taskService.cacheTask();
	}

	@RequiresPermissions("role:list")
	@RequestMapping(value = "/loadTask", method = RequestMethod.GET)
	public void loadTask() {
		taskService.loadTask();
	}

	@RequiresPermissions("role:list")
	@RequestMapping(value = "/createTask", method = RequestMethod.POST)
	public void createTask(@RequestBody Map<String, String> map) {
		String type = map.get("type");
		String sourceId = map.get("sourceId");
		String date = map.get("date");
		taskService.createTask(type,Integer.parseInt(sourceId), date);
	}
	
	@RequiresPermissions("role:list")
	@RequestMapping(value = "/cancelTask", method = RequestMethod.POST)
	public void cancelTask(@RequestBody Map<String, String> map) {
		String type = map.get("type");
		String sourceId = map.get("sourceId");
		taskService.cancelTask(type,Integer.parseInt(sourceId));
	}
	
	@RequiresPermissions("role:list")
	@RequestMapping(value="/serverDate",method = RequestMethod.GET)
	public boolean serverDate(Integer tid,String mode) {
		Date startTime = null;
		if("t".equals(mode)) {
			Tournament t=tournamentService.selectByTid(tid);
			startTime=t.getStartTime();
			
		}else {
			Pvp t=pvpService.selectByTid(tid);
			startTime=t.getStartTime();
		}
		Date cDate=new Date();
		System.out.println(cDate);
		System.out.println(startTime);
		System.out.println(cDate.getTime());
		System.out.println(startTime.getTime());
		return cDate.before(startTime);
	}
}
