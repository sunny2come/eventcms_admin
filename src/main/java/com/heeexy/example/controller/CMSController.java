package com.heeexy.example.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.heeexy.example.service.CMSService;
import com.heeexy.example.service.FileService;
import com.heeexy.example.util.CommonUtil;
import com.heeexy.example.util.excel.ReadExcel;
import com.heeexy.example.util.model.Bot;
import com.heeexy.example.util.model.BotScoreRange;
import com.heeexy.example.util.model.Game;
import com.heeexy.example.util.model.vo.BotScoreRangeVO;

/**
 * @author: jonny
 * @description: CMS相关Controller
 * @date: 2019/02/24 16:04
 */
@RestController
@RequestMapping("/cms")
public class CMSController {

	@Autowired
	private CMSService cmsService;

	private List<BotScoreRange> tempScoreList=null;
	
	ReadExcel re=new ReadExcel();
	/**
	 * 查询列表
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listGame")
	public JSONObject listGame(HttpServletRequest request) {
		return cmsService.listGame(CommonUtil.request2Json(request));
	}
	@RequiresPermissions("role:list")
	@GetMapping("/listGameForder")
	public JSONObject listGameForder(HttpServletRequest request) {
		return cmsService.listGameForder(CommonUtil.request2Json(request));
	}
	
	@RequiresPermissions("role:list")
	@GetMapping("/listAllGame")
	public List<Game> listAllGame(){
		return cmsService.listAllGame();
	}
	@RequiresPermissions("role:list")
	@GetMapping("/listBotGameRange")
	public List<BotScoreRangeVO> listBotGameRange(Integer gameId){
		return cmsService.listBotGameRange(gameId);
	}
	/**
	 * 查询Tournament列表
	 */
	@RequiresPermissions("role:list")
	@GetMapping("/listTournament")
	public JSONObject listTournament(HttpServletRequest request) {
		return cmsService.listTournament(CommonUtil.request2Json(request));
	}

	/**
	 * 修改状态
	 */
	@RequiresPermissions("role:update")
	@PostMapping("/gameOffline")
	public JSONObject gameOffline(@RequestBody Map<String,Integer> map) {
		return cmsService.gameOffline(map.get("gameId"),map.get("business"));
	}
	
	@RequiresPermissions("role:update")
	@GetMapping("/selectProductIdByType")
	public List<JSONObject> selectProductIdByType(Integer type) {
		List<JSONObject> result = cmsService.selectProductIdByType(type);
		return result;
	}
	
	@RequiresPermissions("role:list")
	@GetMapping("/listEvents")
	public PageInfo<JSONObject> listEvents(Integer gameId,Integer pageNum,Integer pageRow) {
		return cmsService.listEvents(gameId,pageNum,pageRow);
	}
	@RequiresPermissions("role:list")
	@PostMapping("/updateEventNumber")
	public boolean updateEventNumber(@RequestBody JSONObject requestJson) {
		return cmsService.updateEventNumber(requestJson);
	}
	@RequiresPermissions("role:list")
	@PostMapping("/updateGameForder")
	public boolean updateGameForder(@RequestBody JSONObject requestJson) {
		return cmsService.updateGameForder(requestJson);
	}
	
	@RequiresPermissions("role:list")
	@GetMapping("/allBotList")
	public List<Bot> allBotList(){
		return cmsService.allBotList();
	}
	
	@RequiresPermissions("role:add")
	@PostMapping("/botScoreUpload")
	@SuppressWarnings("all")
	public List<BotScoreRange> botScoreUpload(MultipartFile file,Integer gameId) {
		List<BotScoreRange> list=re.importExcel(file, BotScoreRange.class,"Second");
		for (BotScoreRange botScoreRange : list) {
			botScoreRange.setGameId(gameId);
		}
		tempScoreList=list;
		return list;
	}
	
	@RequiresPermissions("role:add")
	@PostMapping("/botInfoUpload")
	@SuppressWarnings("all")
	public List<Bot> botInfoUpload(MultipartFile file){
		List<Bot> list = re.importExcel(file,Bot.class,"bot_avatar");
		cmsService.truncateData("bot");
		cmsService.batchInsertBot(list);
		return list;
	}
	
	@RequiresPermissions("role:add")
	@PostMapping("/submitScoreUpload")
	public boolean batchInsertScore() {
		return cmsService.batchInsertScore(tempScoreList);
	}
	
	@RequiresPermissions("role:add")
	@PostMapping("/insertGame")
	public boolean insertGame(@RequestBody Game game) {
		return cmsService.insertGame(game);
	}
	@RequiresPermissions("role:add")
	@PostMapping("/updateGame")
	public boolean updateGame(@RequestBody Game game) {
		return cmsService.updateGame(game);
	}
	
	@Autowired
	FileService fileService;
	
	@RequiresPermissions("role:list")
	@RequestMapping(value = "/iconUpload", method = RequestMethod.POST)
	public String iconUpload(MultipartFile file) {
		return fileService.iconUpload(file);
	}

	//访问服务器图片 正则匹配带.的路径
	@RequiresPermissions("role:list")
	@GetMapping(value = "/icon/{imgName.+}")
	public void getImage(@PathVariable("imgName.+") String imgName, HttpServletRequest request,
			HttpServletResponse response) {

		response.setContentType("image/jpeg");

		Path path = Paths.get("iconImg/" + imgName);
		try {
			byte[] bytes = Files.readAllBytes(path);
			response.getOutputStream().write(bytes);
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
